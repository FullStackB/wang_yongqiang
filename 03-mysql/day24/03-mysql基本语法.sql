-- 启动/关闭mysql服务
net start/stop mysql;

-- 登录mysql
mysql -hlocalhost -P3306 -uroot -proot
mysql -uroot -p空格回车
-- 退出mysql exit; \q

-- 1.1创建数据库
create database user;
-- 1.2删除数据库
drop database user;
-- 1.3修改编码集
alter database user charset=utf8;
-- 2.1显示所有数据库
show databases;
-- 2.2显示匹配数据库(% _)
show databases like 'u%'; 
show databases like '%u';
show database like 'u_';
-- 2.3选择数据库 use user;
-- 3.1.1普通创建表
create table user(name varchar(10), id int(3));
-- 3.1.2设置表的字符集
create table user(name varchar(4), day int(4)) charset=utf8;
-- 3.1.3复制已有表结构
create table a like user;
-- 3.2.1删除表结构
drop table user;
-- 3.2.2删除表字段
alter table user drop id;
-- 3.3.1改表名
rename table users to a;
-- 3.3.2新增片段
alter table add id int(2);
-- 3.3.3修改字段
alter table user change id idc int(4);
-- 3.4.1显示所有数据表
show tables;
-- 3.4.2显示某个数据表创建过程
show create table user;
-- 3.4.3显示数据表结构
desc users;