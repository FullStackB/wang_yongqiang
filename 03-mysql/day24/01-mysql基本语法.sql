-- mysql服务启动
net start mysql
-- mysql服务停止
net stop mysql


-- 登录mysql
-- 语法: mysql -h(mysql的域名或ip) -P(mysql的端口) -u用户名 -p密码
mysql -hlocalhost -P3306 -uroot -proot
mysql -uroot -p 空格回车
-- 退出mysql
-- exit; \q


-- 1.创建数据库
-- 语法: create database 数据库名;
create database users;
-- 2.删除数据库
-- 语法: drop database 数据库名;
drop database users;
-- 3.修改数据库编码集
-- 语法: alter database 数据库名 charset=字符集编码值
alter database users charset=utf-8
-- 4.显示数据库
-- 4.1 显示所有数据库
show databases;
-- 4.2 显示匹配数据库
show databases like '%u';
show databases like 'user%';
-- 4.3 忘字母 用_替代
show databases like 't_ext';
-- 5.选择数据库
use users;


-- ===创建表
-- 1.1 普通创建表
-- 语法：create table 表名(字段名 字段类型 字段属性,字段名 字段类型 字段属性...);
create table inn(name varchar(10), age int(3), sex varchar(2));
-- 设置字符集
create table icc(sex varchar(2), age int(3), name varchar(10)) charset=utf8;
-- 1.2 复制已有表结构
-- 语法: create table 新表名 like 原表名
create table cc like users;
-- ===删除表
-- 2.1删除表结构
-- 语法: drop table 表名[,表名]
drop table info;
drop table pro,pro2; 
-- 2.2 删除表中字段
alter table incc drop id;
-- ===修改表
--3.1 改表名rename table 旧表名 to 新表名
rename table a to b;
-- 3.2 新增字段
-- 语法alter table 表名 add [column] 新字段名 列类型 [列属性] [位置first/after 字段名]
alter table cc add id int(10);
-- 3.3 修改字段名
-- 语法: alter table 表名 change 旧字段名 新字段名 [列属性] [新属性]
alter table cc change user username varchar(100);
-- ===显示数据表
-- 4.1 显示所有数据表
 show tables;
-- 4.2 显示某个数据表 创建过程 
show create table 表名 ; \g \G
-- 4.3 显示数据表结构
-- desc 表名;
desc info;

