-- mysql服务启动
net start mysql;
-- mysql服务停止
net stop mysql;

-- 登录myspl
-- 语法: mysql -hmysql域名或ip -Pmysql端口 -u用户名 -p密码
mysql -hlocalhost -P3306 -uroot -proot
mysql -uroot -p空格回车
-- 退出mysql exit; \q

-- 1.创建数据库 create database 数据库名;
create database users;
-- 2.删除数据库 drop database 数据库名;
drop database users;
-- 3.修改数据库编码集 alter database 数据库名 charset=字符集编码;
alter database users charset=utf8;
-- ===4.显示数据库
-- 4.1 显示所有数据库
show databases like '%u';
-- 4.2 显示匹配数据库
show databases like 'user%';
-- 4.3 下划线代表忘记的字
show databases like 't_st';
-- 5. 选择数据库 use user;


-- ===创建表
-- 1.1普通创建表 create table 表名(字段名 字段类型 字段属性, 字段名 字段类型 字段属性,...);
create table cc(name varchar(10), age int(3), sex varchar(2));
-- 设置字符集
create table cc(name varchar(10), age int(3), sex varchar(2)) charset=utf8;
-- 1.2复制已有表结构 create table 新表名 like 原表名
create table ccc like cc;
-- ===删除表
-- 2.1 删除表结构 drop table 表名[,表名]
drop table cc;
-- 2.2 删除表字段 alter table 表名 drop 字段名;
alter table cc drop id;
-- ===修改表
-- 3.1 改变名 rename table 旧表名 to 新表名;
rename table a to b;
-- 3.2 新增字段 alter table 表名 add [column] 新字段名 列类型 [列属性] [位置first/after 字段名]
alter table b add id int(10);
-- 3.3 修改字段名
-- alter table 表名 change 旧字段名 新字段名 [列属性] [新属性]
alter table cc change id idname varchar(100);
-- 4.1显示所有数据表
show tables;
-- 4.2显示某个数据表 创建过程
show create table 表名 ; \g \G
-- 4.3显示数据表结构
desc users;

