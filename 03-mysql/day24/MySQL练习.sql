-- 1.1启动mysql服务
net start mysql
-- 1.2连接数据库
mysql -hlocalhost -P3306 -uroot -proot;

-- 2.1创建数据库
create database four;
-- 2.2设置数据库字符集
alter database four charset=utf8;
-- 2.3删除数据库
drop database four;

-- 3.1 创建数据表
create table four.user(name varchar(255), age int(3));
-- 3.2 删除表
drop table four.user;

-- 4.1修改表中字段名
alter table a.b change id idc int(3);
-- 4.2增加字段
alter table a.b add name varchar(10);
-- 4.3删除字段
alter table a.b drop idc;

-- 5.1创建数据库
create database czxy;
-- 5.2创建表
use czxy;
create table user(id int(3), username varchar(10), sex varchar(2), Email varchar(10), phong int(2), resume varchar(255));
create table news(id int(3), title varchar(3), gai varchar(4), content varchar(5), thing varchar(4), dianNum int(3), shouNum int(5));

-- 6.1插入字段
alter table user add day int(3) first;
alter table news add fun varchar(4) after id;
-- 6.2插入数据
insert into user(phong) values(12);
insert into news(title) values('我的')


-- 7.1删除字段
alter table user drop id;
-- 7.2删除数据
delete from news where id = 2;