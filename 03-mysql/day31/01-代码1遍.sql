-- having的本质和where一样，是用来进行数据条件筛选的
-- 先找到having_data 的所有数据 按照 性别进行分组 显示  个数
select count(*), sex from having_data  group by sex;
-- 想让 数量大于=6的那一组 显示名字
select count(*),sex from having_data group by sex having count(*) = 4;

-- +、-、*、/、%
-- 使用算术运算符 进行结果运算
-- >、>=、<、<=、=、<>
-- 通常是用来在条件中进行限定结果
-- and、or、not 逻辑运算符
-- && || ! 用于在条件中限定结果

-- 多个或 简写 in(结果1,结果2,结果3)
-- 显示 id为1  3  5 6  8的用户的信息
select * from having_data where id in(1,3,5,6,8);

-- 删除name为小乔  貂蝉 不知火舞这几个人的信息
delete from having_data where name in('小乔','貂蝉','不知火舞');

-- 找到 is_data中int_1为null的数据
select * from is_data where int_1 is null;

-- 查询数据片段联合形成一张表
-- union
select * from union_data where stu_set = '女';
select * from union_data where stu_set = '男';
(select * from union_data where stu_set = '女')
union 
(select * from union_data where stu_set = '男');

(select * from union_data where stu_set = '男')
union
(select * from union_data where stu_height > 150);

-- 交叉查询 外连接(笛卡尔积) cross join
select * from having_data cross join union_data;
select * from having_data;
select * from union_data;
select * from having_data cross join union_data;

-- 内连接 inner join
select * from having_data inner join union_data on having_data.age=union_data.stu_age;


---------
-- 需求: 我知道某一个学生的姓名 想要求出该学生的班级名称
-- select嵌套1 (标量子查询)
select class_name from stu_class where class_id = (select class_id from stu_info where info_name = '虚竹');
-- select嵌套2 (列子查询)
-- 把一个表里 id = 1||id = 2||id = 1||id = 2

select class_name from stu_class where class_id in (select class_id from stu_info); 

-- select 嵌套4种  得到的1个表
-- 主查询(字段)  1/多
-- 子查询(数据条数) 1/多

select * from stu_class where exists(select * from stu_info where stu_class.class_id = stu_info.class_id);

select * from (select * from stu_info order by info_age desc) as temp group by class_id;


-- 子查询
-- 标量子查询
select name from admins where age = (select max(age) from admins);
-- 行子查询
select name,age from admins where age = (select max(age) from admins);
-- 列子查询
select name from admins where id in (2,4,7);
-- 表子查询
select name,age from admins where id in (2,4,7);
-- where exists ()语句会返回存在的

-- 数据库备份
-- mysqldump -h本机地址 -P端口号 -u用户名 -p密码  数据库名> 备份文件地址
mysqldump -hlocalhost -P3306 -uroot -proot dany > C:\Users\Administrator\Desktop\tt\dany.sql;
mysql -uroot -proot aa < C:\Users\Administrator\Desktop\tt\dany.sql
