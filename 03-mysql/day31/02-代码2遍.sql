-- 标量子查询
-- 找出年龄最大学生的id值
select id from admins where age = (select max(age) from admins);

-- 行子查询
-- 找出年龄最大学生的id值和姓名
select id,name from admins where age = (select max(age) from admins);

-- 列子查询
-- 选出年龄小于23的学生姓名
select name from admins where age in (select age from admins where age <= 23);

-- 表子查询
-- 选出年龄小于23的学生姓名,id
select id,name from admins where age in (select age from admins where age <= 23);

-- exists查询 返回结果存在的数据
-- where exists ()语句会返回存在的
select * from stu_class where exists(select * from stu_info where stu_class.class_id = stu_info.class_id);