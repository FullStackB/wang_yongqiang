-- 标量子查询
-- 找出数量最多书的名字
select name from book where num = (select max(num) from book);

-- 列子查询
-- 找出数量大于100的书的名字
select name from book where num in (select num from book where num >100);

-- 行子查询
-- 找出数量最多书的名字和描述
select name,introduction from book where num = (select max(num) from book);

-- 表子查询
-- 找出数量大于100的书的所有信息
select * from book where num in (select num from book where num >100);

-- where exists ()语句会返回存在的，满足条件的
select * from stu_class where exists(select * from stu_info where stu_class.class_id = stu_info.class_id);

