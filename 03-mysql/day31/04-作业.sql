-- 部门表dept 员工表emp

-- 单表查询
-- 1. 查询没有上级的员工全部信息（mgr是空的）
select * from emp where mgr is null;
-- 2. 列出30号部门所有员工的姓名、薪资
select ename,sal from emp where deptno = 30;
-- 3. 查询姓名包含 'A'的员工姓名、部门名称
select emp.ename,dept.dname from emp,dept where ename like '%A%' and emp.deptno=dept.deptno;
-- 4. 查询员工“TURNER”的员工编号和薪资
select empno,sal from emp where ename = 'TURNER';
-- 5. 查询薪资最高的员工编号、姓名、薪资
select empno,ename,sal from emp where sal = (select max(sal) from emp);
-- 6. 查询10号部门的平均薪资、最高薪资、最低薪资
select avg(sal) from (select * from emp where deptno = 10) as temp;
select max(sal) from emp where deptno = 10;
select min(sal) from emp where deptno = 10;

-- 子查询
-- 1.列出薪金比员工“TURNER”多的所有员工姓名（ename）、员工薪资（sal）
select ename,sal from emp where sal > (select sal from emp where ename = 'TURNER');
-- 2.列出薪金高于公司平均薪金的所有员工姓名、薪金。
select ename,sal from emp where sal > (select avg(sal) from emp);
-- 3.列出与“SCOTT”从事相同工作的所有员工姓名、工作名称(不展示Scott的姓名、工作)
select ename,job from emp where job = (select job from emp where ename = 'SCOTT') && ename != 'SCOTT';
-- 4.列出薪金高于30部门最高薪金的其他部门员工姓名、薪金、部门号。
select ename,sal,deptno from emp where sal > (select max(sal) from emp where deptno = 30) && deptno != 30; 
-- 5.查询薪资最高的员工编号、姓名、薪资
select deptno,ename,sal from emp where sal = (select max(sal) from emp);
-- 6.列出薪金高于本部门平均薪金的所有员工姓名、薪资、部门号、部门平均薪资
-- select ename,sal,deptno from emp as a 
-- inner join (select avg(sal) as sal,deptno from emp group by deptno) as b
-- on a.deptno = b.deptno && a.sal > b.sal;


select ta.ename,ta.sal,ta.deptno,tb.avgsal from emp ta,  
(select deptno,avg(sal) avgsal from emp group by deptno)tb   
where ta.deptno=tb.deptno and ta.sal>tb.avgsal ;