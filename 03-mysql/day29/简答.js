/*
 * 1.什么是错误优先的回调函数?
 *  设计一个回调函数的时候，要优先解决错误，然后再传参
 *  例: fs.readfile('./public', (err, result) => {})
 *
 * 2.请说出Node.js特点(3点以上)
 * <1 单线程
 * <2 非阻塞I/O
 * <3 事件驱动 event-driven
 *
 * 3.如何确认node.js是否安装成功?
 * 在命令行输入 node -v指令,不会报错 说明安装成功
 *
 * 4.在node.js中如何导入模块 'template'?
 * const template = require('template')
 *
 * 5.如何使用npm安装第三方包, 例'browser-sync'
 * nmp install browser-sync
 *
 * 6.get和post提交方式的区别?
 * GET把参数包含在URL中，POST通过request body传递参数
 *
 * 7.node 如何绑定端口号和启动服务器
 * 用listen绑定端口号
 *  app.listen( 端口号, 90=> {})
 * 直接运行服务器js文件可启动服务器
 *
 * 8.如何设置响应头,防止中文乱码?
 * res.writeHead(200,{"Content-Type":"text/plain;charset=UTF-8"});
 *
 * 9.node有哪些全局对象或全局变量(至少3点)
 * <1 global process console
 * <2 定时器函数 require
 * <3 _filename _dirname
 *
 * 10.node有哪些定时功能?(至少3点)
 * setTimeout(), clearTimeout(), setInterval(), clearInterval()
 *
 * 12.Node.js中导入模块和导入js文件写法上有什么区别?
 * 导入模块时 直接导入即可 不需要加 ./ 或 ../
 */

