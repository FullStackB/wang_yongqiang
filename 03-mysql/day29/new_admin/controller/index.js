// 处理器模块
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'dany'
});

// 添加全部数据
let getall = (req, res) => {
    connection.query('select * from user', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }
    })
}

// 添加数据
let adduser = (req, res) => {
    connection.query('insert into user(username,password,sex) values("' + req.body.username + '","' + req.body.password + '","' + req.body.sex + '")', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send('ok');
        }
    })
}

// 删除数据
let deluser = (req, res) => {
    connection.query('delete from user where id =' + req.body.id, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send('ok');
        }
    })
}

// 获取1条数据
let getone = (req, res) => {
    connection.query('select * from user where id =' + req.query.id, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }
    })
}

// 编辑1条数据
let changeuser = (req, res) => {
    connection.query('update user set username = "' + req.body.username + '",password = "' + req.body.password + '",sex = "' + req.body.sex + '" where id =' + req.body.id, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send('ok');
        }
    })
    // console.log(req.body);
}

module.exports = {
    getall,
    adduser,
    deluser,
    getone,
    changeuser
}