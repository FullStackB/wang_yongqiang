// 服务器
const express = require('express');
const router = require('./router');
const bodyParser = require('body-parser');
const app = express();

// 配置静态资源
app.use(express.static('./public'));
// 配置bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
// 配置路由
app.use(router);

app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});