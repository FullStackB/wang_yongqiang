// 路由模块
const express = require('express');
const router = express.Router();
const ctrl = require('../controller');

// 获取全部数据
router.get('/getall', ctrl.getall);
// 添加数据
router.post('/adduser', ctrl.adduser);
// 删除数据
router.post('/deluser', ctrl.deluser);
// 获取指定数据
router.get('/getone', ctrl.getone);
// 编辑数据
router.post('/changeuser', ctrl.changeuser);

module.exports = router;
