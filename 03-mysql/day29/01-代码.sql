-- 相对于where having可以针对分数数据进行统计筛选
select count(*) from having_data group by sex having count(*) >= 4;
select group_concat(name),count(*) from having_data group by age having count(*) >=2;

-- 联合查询
(select * from union_data where stu_set ='男')
union
(select * from union_data where stu_set="女");

(select * from union_data where stu_set ='男' order by stu_height asc limit 100)
union
(select * from union_data where stu_set="女" order by stu_height desc limit 100);
-- 交叉查询
select * from having_data cross join union_data;
-- 内连查询
select * from having_data inner join union_data;
select * from having_data inner join union_data on having_data.age=union_data.stu_age;