-- 插入数据
-- 1.1普通的插入数据
insert into user(id,name,age) values(1, 'neo', '12');
-- 1.2插入全部数据简化
insert into user values(1,'dany','12');
-- 1.3插入部分字段
insert into user(id,name) values(3,'wang');

-- 2.1 删除表中所有数据
delete from user;
-- 2.2删除某一个指定条件数据
delete from user where id = 2;

-- 3.1 更新一条数据
update users set age=10;
update users set age=12 where id=10;
-- 3.2更新多条数据 
update users set name='wangyongqiang',age=23;

-- 4.1查询所有数据
select * from user;
select age from users where id=12;