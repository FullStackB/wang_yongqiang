-- 插入数据
insert into user(name,age) values('dany',12);
insert into user values('dany',12);
insert into user(cc) values('cc');

-- 删除数据
delete from user; --删除全部
delete from users where id=1; --删除指定

-- 更新数据
update user set age =10; --修改所有
update users set age=10,sex='man' where id =10; --修改指定数据

-- 查询数据
select * from user;
select age,name,sex from user where id =10;