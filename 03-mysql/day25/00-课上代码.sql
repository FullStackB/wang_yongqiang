-- 1.插入数据
-- 1.1普通的插入数据
-- 语法: insert into 表名(字段列表)values(字段值列表)
insert into users(id,name,age,sex) values(1,'dany',12,'男');
-- 1.2不论顺序的插入数据
insert into users(id,age) values(2,23);
-- 1.3不写字段 把所有信息插入数据
-- 语法: insert into 表名 values(按照字段顺序写的所有信息)
insert into users values(4,'cty',34,'女');


update users set age=23;
update users set age=28 where id=3;

update users set name ='dany',id=3, where id =3;

select * from user;
select name form users;
