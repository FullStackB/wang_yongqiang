-- 登录mysql
mysql -uroot -p
-- 创建数据库
create database students;
-- 使用数据库
use students;
-- 创建数据表
create table stu(id int,name varchar(4),age int,sex varchar(3),hometown varchar(10), tel int, room_num int);
-- 添加22条数据
insert into stu(name,age,sex,hometown,tel,room_num) values('a',10,'boy','Hebei',123,201);
insert into stu(name,age,sex,hometown,tel,room_num) values('b',11,'girl','Shandong',123,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('c',12,'boy','Hebei',123,201);
insert into stu(name,age,sex,hometown,tel,room_num) values('d',10,'girl','Hebei',123,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('e',11,'boy','Shandong',123,201);
insert into stu(name,age,sex,hometown,tel,room_num) values('f',12,'girl','Hebei',1223,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('g',13,'boy','Hebei',123,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('a',14,'girl','Shandong',123,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('b',15,'boy','Hebei',123,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('ac',16,'girl','Hebei',123,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('aa',16,'boy','Shandong',1223,201);
insert into stu(name,age,sex,hometown,tel,room_num) values('e',17,'girl','Hebei',1213,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('f',15,'boy','Hebei',1123,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('g',17,'girl','Hebei',2123,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('b',13,'girl','Shandong',1233,201);
insert into stu(name,age,sex,hometown,tel,room_num) values('c',19,'boy','Hebei',1423,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('b',20,'girl','Shandong',1523,201);
insert into stu(name,age,sex,hometown,tel,room_num) values('ae',25,'boy','Hebei',12523,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('aa',24,'girl','Shandong',1223,201);
insert into stu(name,age,sex,hometown,tel,room_num) values('ad',52,'girl','Hebei',1223,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('ab',54,'boy','Shandong',1223,23);
insert into stu(name,age,sex,hometown,tel,room_num) values('ac',34,'girl','Shandong',123,23);
-- 查询山东同学 
select * from stu where hometown='Shandong';
-- 查询201同学
select * from stu where room_num=201;
-- 查询男生
select * from stu where sex ='boy';
-- tel为123的性别更新为girl
update stu set sex='girl' where tel=123;
-- 删除id为10和15的同学
delete from stu where id =10;
delete from stu where id =15;