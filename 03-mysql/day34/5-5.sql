-- 1.创建student和score表
create table student (
    id int primary key auto_increment comment '学生id',
    name varchar(8) comment '学生姓名',
    native_place varchar(20) comment '学生籍贯',
    age int comment '学生年龄',
    department varchar(10) comment '学生院系'
);

create table score (
    id int primary key auto_increment comment '成绩id',
    c_name varchar(10) comment '科目',
    grade int comment '分数',
    sid int comment '学生id',
    foreign key(sid) references student(id)
);
-- 2.为student表和score表增加记录
insert into student values 
(null,'李四','河北',21,'平面设计系'),
(null,'张文','湖北',22,'计算机系'),
(null,'王伟','山东',25,'平面设计系'),
(null,'陈晨','湖南',25,'计算机系'),
(null,'姜姗','黑龙江',35,'英语系'),
(null,'董庆','内蒙古',23,'平面设计系');

insert into score values
(null,'计算机',78, 6),
(null,'语文',83, 2),
(null,'计算机',40, 3),
(null,'语文',23, 1),
(null,'英语',97, 5),
(null,'计算机',90, 4),
(null,'英语',94, 4);

-- 3.查询student表的所有记录
select * from student;
-- 4.查询student表的第2条到4条记录
select * from student where id in (2,3,4);
-- 5.从student表查询所有学生的学号（id）、姓名（name）和院系（department）的信息
select id,name,department from student;
-- 6.从student表中查询计算机系和英语系的学生的信息
select * from student where department in ('计算机系', '英语系');
-- 7.从student表中查询年龄18~35岁的学生信息
select * from student where age between 18 and 35;
-- 8.查询每个院系有多少人
select department '院系',count(*) '人数' from student group by department;
-- 9.查询每个科目的最高分
select c_name,max(grade) from score group by c_name;
-- 10.查询李四的考试科目（c_name）和考试成绩（grade）
select c_name,grade from score where sid = (select id from student where name = '李四');
-- 11.所有学生的信息和考试信息
select a.name,a.native_place,a.age,a.department,b.c_name,b.grade from 
student a,score b where a.id = b.sid;
-- 12.计算每个学生的总成绩
select a.name,b.sumgrade from 
student a,(select sum(grade) sumgrade,sid from score group by sid) b
where a.id = b.sid;
-- 13.计算每个考试科目的平均成绩
select c_name,avg(grade) from score group by c_name;
-- 14.查询计算机成绩低于95的学生信息
select * from student where id in 
(select sid from score where c_name = '计算机' and grade < 95);
-- 15.查询同时参加计算机和英语考试的学生的信息
select * from student where id in 
(select sid from score group by sid
having group_concat(c_name) like '%英语%' and group_concat(c_name) like '%计算机%');
-- 16.将计算机考试成绩按从高到低进行排序
select grade from score where c_name = '计算机' order by grade desc;
-- 17.查询姓张或者姓王的同学的姓名、院系和考试科目及成绩
select a.name,a.department,b.c_name,b.grade from student a,score b
where (a.name like '%王%' or a.name like '%张%') and a.id = b.sid;
-- 18.查询都是湖南的学生的姓名、年龄、院系和考试科目及成绩
select a.name,a.age,a.department,b.c_name,b.grade from student a,score b
where a.native_place like '湖南' and a.id = b.sid;

