-- 创建独立库
create database test;
-- 1.创建4个表
create table student(
    no varchar(20) primary key comment '学号(主键)',
    name varchar(20) not null comment '学生姓名',
    sex varchar(20) not null comment '学生性别',
    birthday datetime comment '学生出生年月',
    class varchar(20) comment '学生所在班级'
); --null datetime

create table teacher(
    no varchar(20) not null primary key comment '教工编号(主键)',
    name varchar(20) not null comment '教工姓名',
    sex varchar(20) not null comment '教工性别',
    birthday datetime comment '教工出生年月',
    prof varchar(20) comment '职称',
    depart varchar(20) not null comment '教工所在部门'
);

create table course(
    no varchar(20) not null primary key comment '课程号(主键)',
    name varchar(20) not null comment '课程名称',
    t_no varchar(20) not null comment '教工编号(外键)',
    foreign key(t_no) references teacher(no)
); --foreign references

create table score(
    s_no varchar(20) not null comment '学号(外键)',
    c_no varchar(20) not null comment '课程号(外键)',
    degree decimal(4,1) comment '成绩',
    foreign key(s_no) references student(no),
    foreign key(c_no) references course(no)
);

-- 2.1往各表插入数据
insert into student values
('101', '赵军', '男', '1987-03-20 00:00', '95033'),
('103', '毛君', '男', '1984-09-23 00:00', '95031'),
('105', '李明', '男', '1982-11-02 00:00', '95031'),
('107', '范丽', '女', '1987-01-23 00:00', '95033'),
('108', '王华', '男', '1981-09-01 00:00', '95033'),
('109', '张芳', '女', '1983-01-10 00:00', '95031');

insert into teacher values
('804', '王诚', '男', '1957-12-02 00:00', '副教授', '计算机系'),
('825', '张萍', '女', '1971-05-05 00:00', '助教', '计算机系'),
('831', '毛冰', '女', '1975-08-14 00:00', '助教', '电子工程系'),
('856', '李旭', '男', '1966-03-12 00:00', '讲师', '电子工程系');

insert into course values
('3-105', '计算机导论', '825'), 
('3-245', '操作系统', '804'), 
('6-166', '数字电路', '856'), 
('9-888', '高等数学', '831');

insert into score values
('103', '3-105', 92),
('103', '3-245', 86),
('103', '6-166', 85),
('105', '3-105', 88),
('105', '3-245', 75),
('105', '6-166', 79),
('109', '3-105', 76),
('109', '3-245', 68),
('109', '6-166', 81);

-- 2.3 查询student 表中记录 name sex class列信息
select name,sex,class from student;

-- 2.4 查询teacher 表中不重复的 depart 列信息
select distinct depart from teacher;

-- 2.5 查询score表中成绩在70 到90之间所有记录
select * from score where 70 < degree && degree< 90;
select * from score where 70 < degree and degree< 90;


-- 2.6 查询score表中成绩为68, 75或88的记录
select * from score where degree = 68 || degree =75 || degree =88;

-- 2.7 查询student表中 '95031'班 或性别为'女'的记录
select * from student where class = '95031'|| sex = '女';

-- 2.8 以c_no升序 deree降序查询 score表中所有记录
select * from score order by c_no, degree desc;

-- 2.9 查询"95031"班的学生人数
select count(*) from student where class = '95031';

-- 2.10 查询score表中最高分的s_no 和c_no
select max(degree) from score; --最高分为92
select s_no,c_no from score where degree = 92;

-- 2.11 查询每门课的平均成绩
select c_no from score group by c_no;

-- 2.12 查询分数大于60，小于80的s_no列
select s_no from score where 60 < degree && degree< 80;

-- 2.13 查询student表中姓“王”的同学记录
select * from student where name like '王%';

-- 2.14 查询student表中最大和最小的birthday日期值
select max(birthday),min(birthday) from student;

-- 2.15 以年龄从小到大的顺序查询student表中的全部记录
select * from student order by birthday desc;

-- 2.16 查询最高分同学的成绩信息
select max(degree) from score; --最高分为92
select * from score where degree = 92;

-- 2.17 把学号为‘108’，名字为‘王华’学生的班级改为‘95034’
update student set class = '95034' where no = '108' && name = '王华';





