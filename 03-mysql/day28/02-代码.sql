create table tt(
    id int primary key auto_increment,
    name varchar(5) 
);
-- 多数据插入
insert into tt(name) values
('a'),
('b'),
('c'),
('d');

-- 主键冲突(在含主键数据表 更新数据)
--on duplicate key update
insert into tt values(1, 'a') on duplicate key update name = 'aa'; 
insert into tt values(2,'b') on duplicate key update name = 'bb';
replace into tt values(3, 'cc'); --into后没加表名
replace into tt values(4,'dd');

-- 复制表所有内容
create table dd like tt;
insert into dd select * from tt;

-- 限定条数更新数据
insert into tt(name) values('a'),('a'),('a'),('a'),('a');
update tt set name = 'aa' where name = 'a' limit 3;
-- 限定删除数据
delete from tt where name = 'aa' limit 4;

-- 查询不重复数据
select distinct * from tt;
-- 查询的数据 显示改名
select id as dd, name as nn from tt;
-- 动态数据
select * from (select * from tt) as cag;

delete from tt;
alter table tt add sex varchar(3);
insert into tt(name,sex) values
('小强', '男'),
('小红', '女'),
('小刚', '男'),
('冰冰', '女'),
('甄嬛','女'),
('玉帝','男');
-- ground by
 select group_concat(name) from tt group by sex;
 select name,sex,id from tt group by sex desc, id;