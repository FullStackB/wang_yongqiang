-- 插入多条数据
insert into 表名[(字段列表)] values(),(),(),(),()...

-- 更改含主键 数据表的数据
insert into day05 values(14, 'a') on duplicate key update name = 'aa';
insert into day05 values(15, 'b') on duplicate key update name = 'bb';
replace into day05 values(16, 'cc');
replace into day05 values(17, 'dd');

-- 复制表所有数据
create table ab like day05;
insert into ab select * from day05;

-- 修改限定条数
update day05 set name = 'aaaa' where name = 'z' limit 3;
update day05 set name = 'bbbb' where name = 'x' limit 3;
-- 删除限定数据
delete from day05 where name = 'aaaa' limit 2;
delete from day05 where name = 'dd' limit 3;

-- 查询重复元素
select distinct * from day05;
-- 显示名称更改as
select id as d, name as n from day05;
-- 动态数据
select * from (select * from day05) as aaa;
select name from (select * from day05) as aaa;

-- group by
insert into day05(name,sex) values
('dany', '男'),
('aa', '女'),
('ccc', '女'),
('饕餮','男'),
('射手','女'),
('aa', '男');

select name from day05 group by sex;
select group_concat(name),sex from day05 group by sex;

select count(id) from day05; --次数
select sum(id) from day05; --求和
select max(id) from day05; --最大值
select min(id) from day05; --最小值
select avg(id) from day05; --平均值
select * from day05;

-- 降序desc
select group_concat(name),sex from day05 group by sex, id desc;