-- 多数据插入
insert into an(id) values(1),(2),(3),(4);
-- 主键冲突
insert into an(name) values
('a'),('b'),('c'),('d'),('e');

insert into an values('a',1) on duplicate key update name = 'aa';
replace into an values('bb', 2);

-- 蠕虫复制
create table ann like an;
insert into ann select * from an;
select * from ann;

-- limit限定字数
update an set name = 'bb' where name = 'b' limit 3;
delete from an where name = 'bb' limit 4;

-- 查询不重复数据
select distinct * from an;
-- 显示不同名字 
select name as aa, id as dd from an;

delete from an;
alter table an add sex varchar(4);
insert into an(name,sex) values 
('a', '男'), 
('d', '女'), 
('c', '男'), 
('b', '女'), 
('e', '男');
select group_concat(name),sex from an group by sex desc;
select name,sex,id from an group by sex desc, id;

-- 聚合函数
select count(id) from an; --数据条数
select sum(id) from an; --求和
select max(id) from an; --最大值
select min(id) from an; --最小值
select avg(id) from an; --平均值