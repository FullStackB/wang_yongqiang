-- goods 商品表
create table goods (
    id int auto_increment primary key comment '商品ID',
    goods_name varchar(10) not null comment '商品名',
    goods_price float not null comment '商品价格',
    goods_brand varchar(10) not null comment '商品品牌',
    goods_pic varchar(10) not null  comment '商品图片',
    pub_data datetime not null comment '上架时间',
    is_hot int unsigned comment '是否热销 1-100热销 数值越大 排名越靠前',
    is_new int not null default 1 comment '是否上新  1 新品  0 不是新品  默认新品',
    is_show int not null default 1 comment '是否展示  1 展示  0  不展示     默认为1',
    goods_desc text not null comment '商品描述'
);

-- brand 品牌表
create table brand (
    id int auto_increment primary key comment '品牌id',
    brand_name varchar(10) not null default '' comment '品牌名',
    brand_logo varchar(10) not null default '' comment '品牌logo',
    brand_site varchar(10) not null default '' comment '品牌网站',
    brand_desc  varchar(255) not null default '' comment '品牌描述',
    is_show int default 1 comment '是否展示 1展示 0不展示 默认都是展示',
    brand_sort int unsigned comment '品牌排顺 1-100 数字越大 排名越靠前'
);

-- user 用户表
create table user (
    id int auto_increment primary key comment '用户表',
    user_name varchar(10) not null default '' comment '用户名',
    user_tel varchar(11) not null default '' comment '用户手机',
    user_address varchar(255) not null default '' comment '用户地址'
);

-- order 订单表
create table `order` (
    id int auto_increment primary key comment '订单id',
    goods_id int comment '商品id',
    user_id int comment '用户id'
);