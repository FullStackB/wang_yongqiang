-- ===1===
create table account_log(
    log_id mediumint not null auto_increment comment'自增ID号',
    user_id mediumint not null comment"用户登录后保存在session中的id号,跟users表中的user_id对应",
    user_money decimal(10,2) not null comment'用户败笔记录的金额',
    frozen_money decimal(10,2) not null comment'被冻结的资金',
    rank_points mediumint not null comment'等级积分,跟消费积分是分开的',
    pay_points mediumint not null comment'消费积分,跟等级积分是分开的',
    change_time int(10) not null comment'该笔操作发生的时间',
    change_desc varchar(255) not null comment'该笔操作的备注',
    change_type tinyint not null,
    primary key(log_id)
);

-- ===2===
create table ad( 
    ad_id smallint not null  auto_increment comment'自增ID号',
    position_id smallint default 0 comment'站外广告;从1开始代表的是该广告所处的广告位,同表ad_postition中的字段position_id的值',
    media_type tinyint not null default 0 comment'广告类型,0图片;1flash;2代码3文字',
    ad_name varchar(60) not null comment'该条广告的广告名称',
    ad_link varchar(255) not null comment'广告链接地址',
    ad_code text not null comment'广告链接的表现,文字广告就是文字或图片和flash就是它们的地址',
    start_time int(11) not null default 0 comment'广告开始时间',
    end_timer int(11) not null default 0 comment'广告结束时间',
    link_man varchar(60) not null comment'广告联系人',
    link_email varchar(60) not null comment'广告联系人的邮箱',
    link_phone varchar(60) not null comment'广告联系人的电话',
    click_count mediumint not null default 0 comment'该广告点击数',
    enabled tinyint not null default 1 comment'该广告是否关闭;1开启;0关闭',
    primary key(ad_id)
);

-- ===3===
create table admin_action(
    action_id tinyint not null comment'自增ID号',
    parent_id tinyint not null default 0 comment'该id 项的父id,对应本表的action_id字段',
    action_code varchar(20) not null comment'代表权限的英文字符串,对应汉文在语言文件中,如果该字段有某个字符串,就表示有该权限',
    relevance varchar(20) not null 
);

-- ===4===
create table admin_log (
    log_id int not null comment'自增ID号',
    log_time int not null default 0 comment'写日志时间',
    user_id tinyint not null default 0 comment'该日志所记录的操作者id,同admin_user的user_id',
    log_info varchar(255) not null comment'管理操作内容',
    ip_address varchar(15) not null comment'登陆者登录IP'
);

-- ===5===
create table admin_message (
    message_id smallint not null comment'自增id号',
    sender_id tinyint not null default 0 comment'发送该留言的管理员id,同admin_user的user_id',
    receiver_id tinyint not null default 0 comment'接收消息管理员id,同admin_user的user_id,如果是给多个管理员发送,则同一个消息给每个管理员id发送一条',
    sent_time int not null default 0 comment'留言发送时间',
    read_time int not null default 0 comment'留言阅读时间',
    reded tinyint not null default 0 comment'留言是否阅读1已阅读;0未阅读',
    deleted tinyint not null default 0 comment'留言是否已经被删除 1已删除;0未删除',
    title varchar(150) not null comment'留言的主题',
    message text not null comment'留言内容'
);

-- ===6===
create table admin_user (
    user_id smallint not null comment'自增id号,管理员代码',
    user_name varchar(60) not null comment'管理员登录名',
    email varchar(60) not null comment'管理员邮箱',
    password varchar(32) not null comment'管理员登录密码',
    add_time int not null comment'管理员添加时间',
    last_ip varchar(15) not null comment'管理员最后一次登录IP',
    action_id text not null comment'管理员管理权限列表',
    suppliers_id smallint not null default 0,
    todolist longtext null comment'记事本记录的数据',
    role_id smallint null
);


