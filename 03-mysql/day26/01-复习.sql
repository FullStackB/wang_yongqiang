-- 数据库
create database user;
drop database user;
-- 数据表
create table czxy.a (id int(3));
drop table czxy.a;
rename table czxy.a to czxy.b;
-- 字段
alter table user add id int(3);
alter table user drop id;
alter table user change id idc int(3);
-- 显示
show databases;
show databases like '%u';
show databases like 'u%';
show database like 'j_k';