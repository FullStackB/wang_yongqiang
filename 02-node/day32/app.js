const express = require('express');
// 创建express服务器
const app = express();


// 服务器的配置

// 1.设置静态资源
app.use(express.static('public'));
// 2.配置路由
// 2.1 引入路由模块
const route = require('./route');
// 2.2 挂载路由模块
app.use(route);

// 3.配置模板(ejs)
// 3.0 引入 ejs包
const ejs = require('ejs');
// 3.1 设置模板引擎的后缀 ejs
app.set('view engine', 'ejs');
// 3.2 设置模板引擎所使用的模板的路径是 ./views
app.set('views', './views');



// 监听端口 并启动服务
app.listen(3000, () => {
  console.log("http://localhost:3000");
})
