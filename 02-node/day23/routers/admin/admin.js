let express = require("express");
let router = new express.Router();
const mysql = require("../../config/db.js");
const crypto = require('crypto');
// 导入moment模块
const moment = require("moment");
const connection = require('../../config/db');

// 管理员管理首页
router.get('/', function (req, res, next) {
	// 从数据库中查询数据
	connection.query('select * from cc', (err, result) => {
		// 判断是否执行成功
		if (err) {
			console.log(err);
		} else {
			// 加载页面
			res.render('./admin/admin/index.html', { data: result });
		}
	});

});

// 管理员管理添加页面
router.get('/add', function (req, res, next) {
	// 加载页面
	res.render('./admin/admin/add.html');
});

// 管理员的添加功能
router.post("/add", function (req, res, next) {
	let fn = () => {// 没有注册，我们需要插入数据
		connection.query('insert into cc(username,password) values("' + req.body.username + '","' + req.body.password + '")', (err, result) => {
			if (err) console.log(err);
		});
		res.redirect('/admin/');
	}

	// 获取数据
	connection.query('select * from cc', (err, result) => {
		if (err) {
			console.log(err);
		} else {
			for (let key in result) {
				// 判断该用户名是否已经注册
				if (result[key].username == req.body.username) {
					res.send('该账号已注册!!!');
					return;
				}
			}
			fn();
		}
	})
});

// 管理员管理修改页面
router.get('/edit', function (req, res, next) {
	// 接受数据的ID
	// 查询对应数据
	connection.query('select * from cc where id =' + req.query.id, (err, result) => {
		// 判断
		if (err) {
			console.log(err);
		} else {
			// 加载修改页面
			res.render('./admin/admin/edit.html', { data: result[0] });
		}
	});
});


// 管理员数据修改功能
router.post("/edit", function (req, res, next) {
	let fn = () => {//更新密码
		connection.query('update cc set password="' + req.body.password + '"where id =' + req.body.id, (err, result) => {
			if (err) {
				console.log(err);
			}
		});
	}
	// 检测是否输入密码
	if (req.body.password) {
		// 获取数据
		connection.query('select * from cc where id =' + req.body.id, (err, result) => {
			if (err) {
				console.log(err);
			} else {
				// 判断该用户是否修改密码
				if (result[0].password != req.body.password) {
					fn();
				}
			}
		});
		res.send('ok');
	} else {// 留在原页
		res.send('');
	}
});

// 无刷新删除数据
router.get("/ajax_del", function (req, res, next) {
	// 接受对应的数据
	connection.query('delete from cc where id =' + req.query.id, (err, result) => {
		if (err) {
			console.log(err);
		} else {
			res.send('ok');
		}
	});
	// 修改数据
})

module.exports = router;