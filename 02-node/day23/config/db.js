// 导入数据库模块
const mysql = require("mysql");

// 设置数据库连接属性
let connect = mysql.createConnection({
	host: "127.0.0.1",
	port: '3306',
	user: "root",
	password: "root",
	database: "dany"
});

// 开始连接数据库
// connect.connect();

// 抛出
module.exports = connect;