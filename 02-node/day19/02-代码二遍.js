// 1.查询字符串的作用
// const qs = require('querystring');
// let str = 'name=王鹏为&name=安政宁&age=20';
// console.log(qs.parse(str));
// qs.parse() 把地址栏字符串转成对象

// 2.border-parser代码模拟
// const express = require('express');
// const server = express();
// let str = '';
// server.post('/', (req, res) => {
//     req.on('data', (chunk) => {
//         str += chunk;
//     });
//     req.on('end', () => {
//         // console.log(str);
//         // 把url编码 变成正常文字
//         console.log(decodeURI(str));
//     });
// });
// server.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 3.res.send,res.sendFile用法
// res.send可返回 字符串 对象 数组 返回buffer数据
// 以前要在浏览器 返回一个网页 fs.readFile() + res.send() === res.sendFile()

// 4.实现点击连结下载文件
// res.download(源文件路径,保存文件名,回调函数)
// const express = require('express');
// const server = express();
// server.get('/', (reeq, res)=> {
//     res.download('./file/song/aa.flac', 'a.flac', (err)=> {
//         if (err) {
//             res.send(err);
//         }
//     });
// });
// server.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 5.node原生服务器art-template用法
// art-template可以把模板,数据拼接成完成网页
// 5.1
// const http = require('http');
// // 导入art-template path包
// const path = require('path');
// const template = require('art-template');
// const server = http.createServer();
// let arr = [
//     "[上海] \"Higher Brothers 2019恭喜发财 WISH YOU RICH 世界巡演\"",
//     "2019\"绝色\"莫文蔚巡回演唱会—天津站",
//     "[廊坊] 2019中国平安中超联赛第三轮 河北华夏幸福vs上海上港",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月"
// ];

// server.on('request', (req, res) => {
//     res.writeHead(200, { 'Content-type': 'text/html; charset=utf-8' });
//     // 拼接网页 (模板的绝对路径,对象)
//     let result = template(path.join(__dirname, './file/page/ul.html'), { list: arr });
//     res.end(result);
// });
// server.listen(3000, () => {
//     console.log('http:127.0.0.1:3000');
// });
// 5.2
// const http = require('http');
// // 拼接模板和数据
// const template = require('art-template');
// // 模板要是绝对路径
// const path = require('path');
// const server = http.createServer();
let students = [
    {
        name: '张三',
        age: '18',
        sex: "男",
        hobby: '篮球'
    }, {
        name: '张三1',
        age: '18',
        sex: "男",
        hobby: '养马'
    }, {
        name: '张三2',
        age: '18',
        sex: "男",
        hobby: '汤头'
    }, {
        name: '张三3',
        age: '18',
        sex: "男",
        hobby: '抽烟'
    }, {
        name: '张三4',
        age: '19',
        sex: "女",
        hobby: '篮球'
    }
]

// server.on('request', (req, res) => {
//     res.writeHead(200, { 'Contet-type': 'text/html; charset=utf-8' });
//     let result = template(path.join(__dirname, './file/page/student.html'), { list: students });
//     res.end(result);
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// })

// 6.express框架art-template用法
const express = require('express');
const server = express();
// 配置模板后缀名
server.engine('html', require('express-art-template'));
// 配置模板路径
server.set('views', './view');
server.get('/', (req,res)=> {
    res.render('student.html', {list: students});
});
server.listen(3000, ()=> {
    console.log('http://127.0.0.1:3000');    
});