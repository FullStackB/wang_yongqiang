// 0. 根据不同路径返回不同内容
// const express = require('express');
// const server = express();
// server.get('/', (req, res) => {
//     res.send('首页');
// })
// server.get('/about', (req, res) => {
//     res.send('关于页');
// });
// server.get('/join', (req, res) => {
//     res.send('加入我们');
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 1. express.tatic快速托管静态资源
// const express = require('express');
// const server = express();
// // 给一个静态资源地址
// server.use(express.static('file'));
// server.get('/', (req, res) => {
//     res.sendFile(path.join(__dirname, './file/index.html'));
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 2. 处理get方式提交数据
// const express = require('express');
// const server = express();
// server.get('/', (req, res) => {
//     console.log(req.query);
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 3. 处理post方式提交数据
// const express = require('express');
// const bodyParser = require('body-parser');
// const server = express();
// server.use(bodyParser.urlencoded({ extended: false }));
// server.post('/login', (req, res) => {
//     console.log(req.body);
// });
// server.listen(3000, () => {
//     console.log('cc');
// });

// 5. qs.parse()使用
// let str = 'name=王永强&sex=男&age=20&height=170cm';
// const qs = require('querystring');
// let result = qs.parse(str);
// console.log(result);
// qs.parse() 可以把地址栏字符串转对象 返回

// 6. 代码实现 body-parser功能(post提交数据)
// const express = require('express');
// const server = express();
// server.post('/', (req, res) => {
//     let str = '';
//     req.on('data', (chunk) => {
//         str += chunk;
//     });
//     req.on('end', () => {
//         // console.log(str);
//         // 把url编码 变成正常文字
//         console.log(decodeURI(str));
//     });
// });
// server.listen(3000, () => {
//     console.log('cc');
// });

// 7. res.send res.sendFile
// res.send可以发送字符串 对象,数组 buffer
// res.sendFile可以免去 读html文件 写入html文件的麻烦,直接可以写入

// 8. res.download
// res.download(源文件路径,保存文件名,回调函数)
// const express = require('express');
// const path = require('path');
// const server = express();
// server.get('/', (req, res) => {
//     res.download(path.join(__dirname, './file/song/aa.flac'), '1.flac', (err) => {
//         if (err) {
//             console.log(err);
//         }
//     });
// });

// server.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 9. art-template模块引擎

// node原生:
// const http = require('http');
// // 导入path art-template包
// const path = require('path');//模板要绝对路径
// const template = require('art-template'); //拼接网页的
// let arr = [
//     "[上海] \"Higher Brothers 2019恭喜发财 WISH YOU RICH 世界巡演\"",
//     "2019\"绝色\"莫文蔚巡回演唱会—天津站",
//     "[廊坊] 2019中国平安中超联赛第三轮 河北华夏幸福vs上海上港",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月"
// ];

// const server = http.createServer();
// server.on('request', (req, res) => {
//     res.writeHead(200, { 'Content-type': 'text/html;charset=utf-8' });
//     let result = template(path.join(__dirname, './file/page/oo.html'), { list: arr, title: 'cc' });
//     res.end(result);
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// node原生
// const http = require('http');
// const path = require('path');
// const template = require('art-template');
// const server = http.createServer();

let students = [
    {
        name: '张三',
        age: '18',
        sex: "男",
        hobby: '篮球'
    }, {
        name: '张三1',
        age: '18',
        sex: "男",
        hobby: '养马'
    }, {
        name: '张三2',
        age: '18',
        sex: "男",
        hobby: '汤头'
    }, {
        name: '张三3',
        age: '18',
        sex: "男",
        hobby: '抽烟'
    }, {
        name: '张三4',
        age: '19',
        sex: "女",
        hobby: '篮球'
    }
]

// server.on('request', (req, res) => {
//     res.writeHead(200, { 'Content-type': 'text/html; charset=utf-8' });
//     if (req.url == '/') {
//         let result = template(path.join(__dirname, './file/page/c.html'), { students: students });
//         res.end(result);
//     }
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

const express = require('express');
const path = require('path');
const server = express();
// 配置模板后缀
server.engine('html', require('express-art-template'));
// 配置模板路径
server.set('views', './view');
server.get('/', (req, res) => {
    res.render('c.html', { students: students });
});
server.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});