// 1.查询字符串的作用
// const qs = require('querystring');
// const str = 'name=王永强&age=20&sex=男';
// console.log(qs.parse(str));
// qs.parse() 把地址栏字符串转成对象

// 2.border-parser代码实现
// const express = require('express');
// const server = express();
// let str = '';
// // data + end + decodeURI == border-parser
// server.post('/', (req, res) => {
//     req.on('data', (chunk) => {
//         str += chunk;
//     });
//     req.on('end', () => {
//         // console.log(str);
//         console.log(decodeURI(str));
//     });
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 3.res.send res.sendFile
// res.send可以给浏览器传字符串 数组对象 buffer数据
// res.sendFile 省一步读取数据 可以直接写入

// 4.实现点击连接下载文件
// res.download(源文件地址,保存文件名,错误函数)
// const express = require('express');
// const server = express();
// server.get('/', (req, res)=> {
//     res.download('./file/song/aa.flac', '像我这样的人', (err)=> {
//         if (err) {
//             console.log(err);
//         }
//     });
// });
// server.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 5.node 原生art-template
// 5.1
// const http = require('http');
// const server = http.createServer();
// // 导入 art-template包(拼接模板和数据)
// const template = require('art-template');
// // 模板要用绝对路径
// let arr = [
//     "[上海] \"Higher Brothers 2019恭喜发财 WISH YOU RICH 世界巡演\"",
//     "2019\"绝色\"莫文蔚巡回演唱会—天津站",
//     "[廊坊] 2019中国平安中超联赛第三轮 河北华夏幸福vs上海上港",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月"
// ];

// const path = require('path');
// server.on('request', (req, res) => {
//     res.writeHead(200, { 'Content-type': 'text/html;charset=utf-8' });
//     if (req.url == '/') {
//         let result = template(path.join(__dirname, './file/page/03ul.html'), { list: arr });
//         res.end(result);
//     }
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });
// 5.2
// const http = require('http');
// const template = require('art-template');
// const path = require('path');
// const server = http.createServer();
let students = [
    {
        name: '张三',
        age: '18',
        sex: "男",
        hobby: '篮球'
    }, {
        name: '张三1',
        age: '18',
        sex: "男",
        hobby: '养马'
    }, {
        name: '张三2',
        age: '18',
        sex: "男",
        hobby: '汤头'
    }, {
        name: '张三3',
        age: '18',
        sex: "男",
        hobby: '抽烟'
    }, {
        name: '张三4',
        age: '19',
        sex: "女",
        hobby: '篮球'
    }
]

// server.on('request', (req, res) => {
//     res.writeHead(200, { 'Content-type': 'text/html;charset=utf-8' });
//     if (req.url == '/') {
//         let result = template(path.join(__dirname, './file/page/03table.html'), { list: students });
//         res.end(result);
//     }
// });

// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });


// 6.express使用模板引擎
const express = require('express');
const server = express();
// 配置模板后缀
server.engine('html', require('express-art-template'));
// 配置模板路径
server.set('views', './view');
server.get('/', (req, res) => {
    res.render('./03.html', { list: students });
});
server.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});
