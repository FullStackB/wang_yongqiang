// 1. 导入fs模块,path模块
const fs = require('fs');
const path = require('path');
// 2. 读取内容
fs.readFile(path.join(__dirname, './file/01-read.txt'), 'utf8', (err, date)=> {
    if (err) {
        return console.log('读取内容失败'+ err.message);
    }
    console.log(date);
});
