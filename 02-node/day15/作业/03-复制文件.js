const fs = require('fs');
const path = require('path');

// 1. 写入文件
fs.writeFileSync(path.join(__dirname, './file/03-copy.txt'), '我准备拷贝文件');
// 2. 追加内容
fs.appendFileSync(path.join(__dirname, './file/03-copy.txt'), '我继续追加信息到文件中');
// 3. 获取文件信息
fs.statSync(path.join(__dirname, './file/03-copy.txt'), (err, stats) => {
    if (err) {
        return console.log('获取信息失败');
    }
    // 3.1 文件大小
    console.log(stats.size);
    // 3.2 创建时间
    console.log(stats.birthtime);
    // 3.3 判断是不是一个文件
    console.log(stats.isFile());
    // 3.4 判断是不是一个目录
    console.log(stats.isDirectory());
});
// 4. 复制文件
fs.copyFileSync(path.join(__dirname, './file/03-copy.txt'), path.join(__dirname, './file/03-copyFile.txt'));