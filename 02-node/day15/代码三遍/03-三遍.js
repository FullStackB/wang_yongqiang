// 1.整理成绩
// const fs = require('fs');
// const path = require('path');
// fs.readFile(path.join(__dirname, './file/1.txt'), 'utf8', (err, date) => {
//     if (err) return console.log(err.message);
//     let str = date.split(' ').filter((item) => {
//         return item.length > 1;
//     });
//     str = str.map((item) => {
//         return item.replace('=', ':');
//     }).join('\n');

//     fs.writeFile(path.join(__dirname, './file/4.txt'), str, err => {
//         if (err) console.log(err.message);
//     });
// });

// 2. path 模块
// path.sep  path.dirname path.extname path.join 
// 3. 同步异步
// const fs = require('fs');
// const path = require('path');

// console.log(1);
// let first = fs.readFileSync(path.join(__dirname, './file/3.txt'), 'utf8');
// console.log(first);
// console.log(1);
// let second = fs.readFileSync(path.join(__dirname, './file/1.txt'), 'utf8');
// console.log(second);
// console.log(1);
// 同步有顺序解构 按顺序执行
// 延时定时器的异步操作
// console.log(1);
// setTimeout(()=>{
//     console.log(2);
// },0);
// console.log(3);

// 4. 模块化
// 模块化是一种明文规定
// 为了降低沟通成本 为了解决代码之间的依赖关系
// node.js 实现了CommonJs规范
// require() 函数入口
// exports() 函数出口
// module 在Node中一个js就是一个文件

// 5. CommonJs中作用域问题
// CommonJs中规定 一个js文件就是一个模块
// 模块中有exports 输出 有require输入
// 除了输入和输出 其中在js文件中的任何变量 方法不能被外界访问
// node开发中无全局作用域 最大的就是模块
const ff = require('./00-test');
ff.fn();
console.log(ff.abag);
