// 需求:
// 小红：99
// 小白：100
// 小黄：70
// 小黑：66
// 小绿：88
// 1. ====整理成绩
// const fs = require('fs');
// const path = require('path');
// // 1.先读取内容
// fs.readFile(path.join(__dirname, './file/1.txt'), 'utf8', (err, date) => {
//     if (err) return console.log('读取内容失败');
//     // 2.1 字符串->数组 + 过滤
//     let str = date.split(' ').filter((item) => {
//         return item.length > 1;
//     });
//     // 2.3 数组替换->字符串
//     str = str.map((item) => {
//         return item.replace('=', ':');
//     }).join('\n');
//     console.log(str);
//     // 3.写入新文件
//     fs.writeFile(path.join(__dirname, './file/2.txt'), str, err => {
//         if (err) console.log('文件写入失败');
//     });
// });

// 2. path 模块
// const path = require('path');
// console.log(path.sep);//返回当前系统片段符
// console.log(path.dirname(__dirname));//返回指定文件所在目录
// console.log(path.extname(__filename));//返回文件后缀名
// path.join();//拼接路径

// 3. 同步异步
// const fs = require('fs');
// const path = require('path');
// let a = fs.readFileSync(path.join(__dirname, './file/2.txt'),'utf8');
// console.log(a);
// 同步有顺序解构 而且有先后

// 异步任务的执行
// console.log('a');
// setTimeout(() => {
//     console.log('b');
// }, 0);
// console.log('c');

// 4. 模块化
// what： 一种明文规定  
// why： 为了降低沟通成本 为了解决代码之间的依赖关系
// how： node.js 实现了CommonJs规范 
//       require() 函数入口
//       exports() 函数出口
//       module 在Node中一个js文件就是一个模块

// 5.CommonJs中作用域问题
// CommonJs中规定 一个js就是一个模块
// 模块中有exports输出 有require输入
// 除了输入和输出 其中在js文件中的任何变量 方法不被外界所访问
// node开发中没有全局作用域 最大就是模块
// const tt = require('./00-test');
// tt.fn();
// console.log(tt.a);
// 在模块exports对象里写入方法 和变量供外界使用

const tt = require('./00-test');
tt.fn();
