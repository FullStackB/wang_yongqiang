// 1. 整理作业
// const fs = require('fs');
// const path = require('path');
// // <1 读取内容
// fs.readFile(path.join(__dirname, './file/1.txt'), 'utf8', (err, date) => {
//     if (err) return console.log(err.message);
//     // <2 操作字符串
//     let str = date.replace(/=/g, ':').split(' ').filter((item) => {
//         return item.length > 0;
//     }).join('\n');

//     // <3 写入文件
//     fs.writeFile(path.join(__dirname, './file/3.txt'), str, (err) => {
//         if (err) console.log(err.message);
//     });
// });

// 2. path模块
// const path = require('path');
// console.log(path.sep);//返回当前系统片段符
// console.log(path.dirname(__filename));//返回文件所在目录
// console.log(path.extname(__filename));//返回后缀名
// path.join();//拼接路径

// 3. 同步异步
// const fs = require('fs');
// const path = require('path');
// console.log(1);
// let a = fs.readFileSync(path.join(__dirname, './file/3.txt'), 'utf8');
// console.log(a);
// console.log(3);
// let b = fs.readFileSync(path.join(__dirname, './file/1.txt'), 'utf8');
// console.log(b);
// console.log(3);
// 同步有顺序解构 有先后

// 延时定时器的异步情况
// console.log(1);
// setTimeout(() => {
//     console.log(2);
// }, 0);
// console.log(3);

// 4. 模块化
// 模块化是一种规范 明文规定
// 模块化是为了降低沟通成本 为了解决代码之间的依赖关系
// node.js 实现了 CommonJs规范
// require() 函数入口
// exports() 函数出口
//module 在Node中一个文件就是一个模块 

// 5. CommonJs中作用域问题
// CommonJs中规定 一个js是一个模块
// 模块中有exports输出 有require输入
// 除了输入和输出 其中在js文件中的任何变量 方法不被外界所访问
// node 开发中没有全局作用域 最大就是模块
const m02 = require('./00-test');
m02.fn();
console.log(m02.cc);