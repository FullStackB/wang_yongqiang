// 1. ==========
// let a = '我是模块内定义的变量';
// let fn = () => {
//     console.log('我是模块内定义的方法');
// }

// // 把模块内变量 方法暴露给外部
// module.exports = {
//     a,
//     fn
// };

// let num = 1;
// let fn = () => {
//     console.log('我是模块内定义的方法!');
// };

// // 可以用exprots向外暴露成员
// module.exports.num = num;
// module.exports.fn = fn;


// 2. =======
// let cc = '模块中的属性和方法如果不进行暴露外界无法访问';
// let fn = () => {
//     console.log('在exports对象中对变量方法进行暴露');
// }
// // 模块出口写法
// // module.exports = {
// //     cc: cc,
// //     fn: fn
// // };

// // 模块出口简写
// exports = {
//     cc,
//     fn
// };

// 3. ===========
let abag = '模块中属性和方法通过放在exports出口上被外界访问';
let fn = () => {
    console.log('完蛋');
}
module.exports = {
    abag,
    fn
};