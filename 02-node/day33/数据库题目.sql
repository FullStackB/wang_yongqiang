-- 1. 使用SHOW语句找出在服务器上当前存在什么数据库：
show databases;
-- 2、创建一个数据库MYSQLDATA
create database mysqldata;
-- 3、选择你所创建的数据库
use mysqldata;
-- 4、查看现在的数据库中存在什么表
show tables;
-- 5、创建一个数据库表
create table a(id int primary key auto_increment);
-- 6、显示表的结构：
desc a;
-- 7、往表中插入一条数据
insert into a values(1);
-- 8、删除表
drop table a;
-- 9、清空表
delete from a;
-- mysql>delete from MYTABLE;
-- 12、更新表中一条数据
update a set id =12 where id = 1;

-- 1.创建student和score表
-- 2.为student表和score表增加记录

-- 3.查询student表的所有记录
select * from student;
-- 4.查询student表的第2条到4条记录
select * from student where id between 902 and 904;
-- 5.从student表查询所有学生的学号（id）、姓名（name）和院系（department）的信息
select id,name,department from student;
-- 6.从student表中查询计算机系和英语系的学生的信息
select * from student where department = '英语系';
-- 7.从student表中查询年龄18~22岁的学生信息
select * from student where (2019-birth) between 18 and 22;
-- 8.从student表中查询每个院系有多少人 
select department,count(*) from student group by department;
-- 9.从score表中查询每个科目的最高分
select c_name,max(grade) from score group by c_name;
-- 10.查询李四的考试科目（c_name）和考试成绩（grade）
select c_name,grade from score where stu_id = (select id from student where name = '李四');
-- 11.用连接的方式查询所有学生的信息和考试信息
select * from student,score where student.id = score.stu_id;
-- 12.计算每个学生的总成绩
select name,sumscore from student a, 
(select stu_id,sum(grade) sumscore from score group by stu_id) b
where a.id = b.stu_id;
-- 13.计算每个考试科目的平均成绩
select c_name,avg(grade) from score group by c_name;
-- 14.查询计算机成绩低于95的学生信息
select * from student where id in
(select stu_id from score where c_name = '计算机' and grade < 95);
-- 15.查询同时参加计算机和英语考试的学生的信息
select * from student where id in
(select stu_id from (select stu_id,group_concat(c_name) ss from score group by stu_id) a
where a.ss like '%计算机%' and a.ss like '%英语%');
-- 16.将计算机考试成绩按从高到低进行排序
select grade from score where c_name = '计算机' order by grade;
-- 17.从student表和score表中查询出学生的学号，然后合并查询结果
select * from student,score where student.id = score.stu_id;
-- 18.查询姓张或者姓王的同学的姓名、院系和考试科目及成绩
select a.name,a.department,b.c_name,b.grade 
from student a, score b 
where a.id in (select id from student where name like '张%' or name like '王%') and a.id = b.stu_id;
-- 19.查询都是湖南的学生的姓名、年龄、院系和考试科目及成绩
select a.name,a.department,b.c_name,b.grade 
from student a, score b 
where a.id in (select id from student where address like '%湖南%') and a.id = b.stu_id;