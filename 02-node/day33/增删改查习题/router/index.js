// 路由模块
const express = require('express');
const router = express.Router();
// 引入处理器
const ctrl = require('../controller');

// 获取所有用户
router.get('/getusers', ctrl.getusers);

// 添加用户
router.post('/adduser', ctrl.adduser);

// 删除用户
router.post('/deluser', ctrl.deluser);

// 搜索用户
router.get('/getSearch', ctrl.getSearch);

// 获取一条数据
router.get('/getone', ctrl.getOne);
// 修改一条数据
router.post('/changeuser', ctrl.changeUser);

module.exports = router;