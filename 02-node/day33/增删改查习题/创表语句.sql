create table users (
    id int primary key auto_increment,
    username varchar(8) comment '用户名',
    realname varchar(8) comment '真实姓名',
    email varchar(25) comment '邮箱',
    telephone varchar(11) comment '电话',
    area varchar(10) comment '地区',
    access enum('管理员','普通用户','游客') comment '权限',
    statius enum('正常','禁用') comment '状态'
);

insert into users values
(null,'越狱人','萧敬腾','1313@qq.com','134567','济南大学',2,1),
(null,'阿斯顿','汪峰','15143@qq.com','452367','北京大学',2,1),
(null,'浅唱低吟','张磊','6713@qq.com','189657','上海大学',2,1),
(null,'凭栏处','姚贝娜','1783@qq.com','134457','石家庄大学',2,1),
(null,'甜酱','平安','39083@qq.com','3465667','深圳大学',2,1),
(null,'bodyQ','戴安娜','23423@qq.com','786567','广州大学',2,1);