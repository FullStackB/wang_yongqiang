// 服务器模块
const express = require('express');
const app = express();

// 设置静态资源目录
app.use(express.static('./public'));

// 设置post解析
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));

// 设置路由
const router = require('./router');
app.use(router);

app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});