// 处理器模块
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'dany'
});

// 获取全部用户
module.exports.getusers = (req, res) => {
    connection.query('select * from users', (err, result) => {
        if (err) console.log(err);
        else res.json(result);
    });
}

// 添加用户
module.exports.adduser = (req, res) => {
    // console.log(req.body);
    let arr = [req.body.username, req.body.realname, req.body.email,
        req.body.telephone, req.body.area, req.body.access, req.body.statius
    ];
    connection.query('insert into users values(null,?,?,?,?,?,?,?)', arr, (err, result) => {
        if (err) {
            console.log(err);
            res.json({
                code: '1001',
                msg: '用户添加失败!'
            });
        } else {
            res.json({
                code: '1000',
                msg: '新用户已添加!'
            });
        }
    });
}

// 删除用户
module.exports.deluser = (req, res) => {
    connection.query('delete from users where id=' + req.body.id, (err, result) => {
        if (err) {
            console.log(err);
            res.json({
                msg: '删除用户失败!!'
            });
        } else {
            res.json({
                msg: '删除用户成功!!'
            });
        }
    });
}

// 搜索用户
module.exports.getSearch = (req, res) => {
    let key = req.query.keyword;
    let sql = 'select * from users where username like "%' + key + '%" or area like "%' + key + '%" or realname like "%' + key + '%" or telephone like "%' + key + '%"';
    connection.query(sql, (err, result) => {
        if (err) console.log(err);
        else res.json(result);
    });
}

// 获取一条数据
module.exports.getOne = (req, res) => {
    connection.query('select * from users where id=' + req.query.id, (err, result) => {
        if (err) console.log(err);
        else res.json(result[0]);
    });
}
// 修改一条数据
module.exports.changeUser = (req, res) => {
    // console.log(req.body);
    let arr = [req.body.username, req.body.realname, req.body.email,
        req.body.telephone, req.body.area, req.body.access, req.body.statius
    ];
    let sql = 'update users set username=?,realname=?,email=?,telephone=?,area=?,access=?,statius=? where id ='+req.body.id;
    connection.query(sql, arr, (err, result) => {
        if (err) {
            console.log(err);
            res.json({
                code: '1001',
                msg: '修改用户失败!'
            });
        } else {
            res.json({
                code: '1000',
                msg: '该用户已修改!'
            });
        }
    });
}