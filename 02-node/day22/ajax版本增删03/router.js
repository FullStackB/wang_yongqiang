// 路由模块

const express = require('express');
const ctrl = require('./contorller');
const router = express.Router();

// 显示注册页
router.get('/', ctrl.showAdd);
// 增添数据
router.post('/adduser', ctrl.adduser);
// 获取数据
router.get('/getuser', ctrl.getuser);
// 删除数据
router.post('/deluser', ctrl.deluser);

module.exports = router;