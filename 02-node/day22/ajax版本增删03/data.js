// mysql 模块

const mysql = require('mysql');
// 建立连接
const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'dany'
});

module.exports = connection;
