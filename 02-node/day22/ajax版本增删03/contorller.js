// 处理器模块

const connection = require('./data');


// 显示注册页
let showAdd = (req, res) => {
    res.sendFile('./views/add.html', { root: __dirname });
}
// 增添数据
let adduser = (req, res) => {
    connection.query('insert into users(username,password) values("' + req.body.username + '", "' + req.body.password + '")', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回成功添加信号!
            res.send('ok');
        }
    });
}
// 获取数据
let getuser = (req, res) => {
    connection.query('select * from users', (err, result) => {
        if (err) {
            console.log(err);
        } else {//返回数据
            res.send(result);
        }
    });
}
// 删除数据
let deluser = (req, res) => {
    connection.query('delete from users where id =' + req.body.id, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回删除成功信号!
            res.send('ok');
        }
    });
}

module.exports = {
    showAdd,
    adduser,
    getuser,
    deluser
};