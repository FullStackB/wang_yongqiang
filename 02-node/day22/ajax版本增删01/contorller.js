// 处理器模块
const connection = require('./data');

module.exports.showAdd = (req, res) => {
    res.sendFile('./views/add.html', { root: __dirname });
}

// 增添数据
module.exports.adduser = (req, res) => {
    connection.query('insert into users(username,password) values("' + req.body.username + '","' + req.body.password + '")', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 传递添加成功信息!
            res.send('ok');
        }
    });
}

// 获取数据
module.exports.getuser = (req, res) => {
    connection.query('select * from users', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 往前传递数据
            res.send(result);
        }
    });
}

// 删除数据
module.exports.deluser = (req, res) => {
    // console.log(req.body);
    connection.query('delete from users where id=' + req.body.id, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 传递成功删除信息
            res.send('ok');
        }
    });
}