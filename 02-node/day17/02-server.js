// 1.基本服务器
// 导入http包
// const http = require('http');
// // 创建服务器
// const server = http.createServer();
// // 给服务器加 request事件
// server.on('request', (req, res) => {
//     res.end('We are the world!');
// });
// // 让服务器监听端口
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 2.在服务器上写中文
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res) => {
//     res.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });
//     res.write('世界第一等');
//     res.end();
// })
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 3.根据不同地址写入不同内容
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res)=> {
//     res.writeHead(200, {"Content-Type":"text/html;charset=utf-8"});

//     if(req.url == '/a') {
//         res.end('我的世界开始下雪');
//     } else if (req.url == '/b'){
//         res.end('沉迷学习,日渐消瘦');
//     } else {
//         res.end('一切还未开始,一切都已结束');
//     }
// });
// server.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 4.根据不同地址显示不同网页

const path = require('path');
const fs = require('fs');
const http = require('http');

const server = http.createServer();
server.on('request', (req, res)=> {
    res.writeHead(200, {"Content-Type": "text/html;charset=utf-8;"});

    // 不同地址 不同网页
    if (req.url == '/' || req.url == '/a') {
        res.end(fs.readFileSync(path.join(__dirname, './file/a.html'),'utf-8'));
    } else if (req.url == '/b') {
        res.end(fs.readFileSync(path.join(__dirname, './file/b.html'), 'utf-8'));
    } else {
        res.end(fs.readFileSync(path.join(__dirname, './file/c.html'),'utf-8'));
    }
});

server.listen(3000, ()=> {
    console.log('http://127.0.0.1:3000');
});