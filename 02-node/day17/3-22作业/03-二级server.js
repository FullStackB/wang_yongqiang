// 1.创建服务器
const http = require('http');
const server = http.createServer();

// 2.给服务器添加request事件
server.on('request', (req, res) => {
    // 2.1 规定格式
    res.writeHead(200, { "Content-type": "text/html;charset=utf-8" });
    // 2.2 根据地址 写入不同内容
    if (req.url == '/home.html' || req.url == '/') {
        res.end('首页');
    } else if (req.url == '/study.html') {
        res.end('学习');
    } else if (req.url == '/us.html') {
        res.end('我们');
    } else {
        res.end('页面找不到');
    }
});

// 3.让服务器监听端口
server.listen(8899, () => {
    console.log('请访问: http://127.0.0.1:8899');
});