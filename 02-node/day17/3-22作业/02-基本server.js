// 1. 引入http模块
const http = require('http');
// 2. 创建服务器
const server = http.createServer();
// 3. 给服务器加个 request
server.on('request', (req, res)=> {
    // 3.1规定输出格式
    res.writeHead(200 ,{"Content-type": "text/html;charset=utf-8"});
    // 3.2 返回内容
    res.end('Hello 传智学院');
});
// 4. 让服务器监听端口
server.listen(8899, ()=> {
    console.log('请访问: http://127.0.0.1:8899');
});