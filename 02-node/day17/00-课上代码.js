// 写一个简单服务器
// 1. 导入http模块
// 2. 使用http模块 创建一个服务器
// 3. 使用服务器监听客户端的请求 
// 4. 使用服务器监听 3000端口


// 1.
const http = require('http');
// 2.
const server = http.createServer();
// 3.
server.on('request', (req, res) => {
    res.write('what \n');
    res.write('tmd');
    res.end();
});
// 4.
server.listen(3000, () => {
    console.log('请访问:  http://127.0.0.1:3000');
});