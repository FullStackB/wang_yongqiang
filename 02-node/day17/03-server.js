// 1. 基本服务器
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res) => {
//     res.end('Wo shi wangyongqiang!');
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 2. 服务器里写中文
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res)=> {
//     res.writeHead(200, {"Content-Type": "text/html;charset=utf-8"});
//     res.write('忽而嘿哟,忽而黑油');
//     res.end();
// });
// server.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 3. 根据不同地址显示不同文字
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res) => {
//     res.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });
//     if (req.url == '/' || req.url == '/a') {
//         res.end('安政宁');
//     } else if (req.url == '/b') {
//         res.end('王鹏为');
//     } else if (req.url == '/c') {
//         res.end('迟天宇');
//     }
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });


// 4. 根据不同地址形式不同网页
const http = require('http');
const path = require('path');
const fs = require('fs');

const server = http.createServer();
server.on('request', (req, res) => {
    res.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });

    let str = '';
    if (req.url == '/' || req.url == '/a') {
        str = fs.readFileSync(path.join(__dirname, './file/a.html'), 'utf-8');
    } else if (req.url == '/b') {
        str = fs.readFileSync(path.join(__dirname, './file/b.html'), 'utf-8');
    } else {
        str = fs.readFileSync(path.join(__dirname, './file/c.html'), 'utf-8');
    }

    res.end(str);
});
server.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});