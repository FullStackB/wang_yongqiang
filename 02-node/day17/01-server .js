// 1.1 Npm 是一个第三方模块的托管网站, npm是Node的包管理工具
// 1.2 全局包: 安装在计算机全局环境中的包, 安装在:C:Users\用户目录\AppData\Roaming\npm
// 安装命令: npm install 包名 -g
// 卸载命令: pm uninstall 包名 -g
// 1.3 本地包: 跟着项目安装的包, 安装在node_modules目录下
//     空项目 必须在当前目录下 先运行npm init 或npm init -y 命令,初始化package.json的配置文件
//     安装本地包命令: npm i 包名
//     pachage-lock.json文件记录了曾经装过包的下载地址, 方便下次直接下载包
//     卸载本地包: npm uninstall/remove 包名 -S/-D
// 1.4 解决下包慢的问题: 在运行js的文件夹中添加一个配置文件 
// .npmrc 内容 registry = 'https://registry.npm.taobao.org

// 2.  IP：服务器在网络中的唯一标识(身份证)
//     域名：IP的名字(IP难记，为了方便用户找到网站)
//     端口: 用具体的数字来区分不同的功能 0-65535 一共65536个端口号

// 3.1 写一个基本服务器
// 3.1.1 导入http模块
// const http = require('http');
// // 3.1.2 使用createServer
// var server = http.createServer();
// // 3.1.3 服务器on 
// server.on('request', (req, res) => {
//     res.write('xxx.xx.x');
//     res.end();
// })
// // 3.1.4 监听端口
// server.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 3.2 在服务器中返回中文
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res) => {
//     res.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });
//     res.end('我的世界开始下雪');
// });
// server.listen(300, () => {
//     console.log('http://127.0.0.1:300');
// });


// 3.3 根据不同路径 返回不同文本内容
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res) => {
//     // 规定写入 内容的类型
//     res.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });

//     // 获取请求地址
//     let src = req.url;
//     if (src == '/a.html' || src == '/') {
//         res.write('新的风暴已经出现');
//     } else if (src == '/b.html') {
//         res.write('怎么能够停滞不前');
//     } else if (src == '/c.html') {
//         res.write('我的世界开始下雪');
//     }

//     res.end();
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });


// 3.4 根据不同路径 显示不同页面
const http = require('http');
const fs = require('fs');
const path = require('path');
const server = http.createServer();
server.on('request', (req, res) => {
    res.writeHead(200, { "Content-Type": "text/html; charset:utf-8;" });

    let str = '';
    // 根据路径写入内容
    if (req.url == '/' || req.url == '/a') {
        str = fs.readFileSync(path.join(__dirname, './file/a.html'), 'utf-8');
    } else if (req.url == '/b') {
        str = fs.readFileSync(path.join(__dirname, './file/b.html'), 'utf-8');
    } else {
        str = fs.readFileSync(path.join(__dirname, './file/c.html'), 'utf-8');
    }

    res.end(str);
});
server.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});