// 处理函数模块 

let showIndex = (req, res) => {
    res.render('cc.html', {
        title: '首页', content: '我是首页', list: [
            {
                a: '关于页',
                b: '/about'
            },
            {
                a: '音乐',
                b: '/music'
            }
        ]
    });
}

let shouAbout = (req, res) => {
    res.render('cc.html', {
        title: '关于页', content: '我是关于页', list: [
            {
                a: '首页',
                b: '/'
            },
            {
                a: '音乐',
                b: '/music'
            }
        ]
    });
}

let showMusic = (req, res) => {
    res.render('cc.html', {
        title: '音乐页', content: '我是音乐页', list: [
            {
                a: '关于页',
                b: '/about'
            },
            {
                a: '首页',
                b: '/'
            }
        ]
    });
}

module.exports = {
    shouAbout,
    showMusic,
    showIndex
}