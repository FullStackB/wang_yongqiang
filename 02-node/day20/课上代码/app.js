// 1. 模板引擎
// const express = require('express');
// const app = express();

// // 配置模板
// app.engine('html', require('express-art-template'));
// app.set('views', './views');
// // engin,set + render(模板,数据)

// app.get('/', (req, res) => {
//     res.render('cc.html', {
//         title: '首页', content: '我是首页', list: [
//             {
//                 a: '关于页',
//                 b: '/about'
//             },
//             {
//                 a: '音乐',
//                 b: '/music'
//             }
//         ]
//     });
// });

// app.get('/about', (req, res) => {
//     res.render('cc.html', {
//         title: '关于页', content: '我是关于页', list: [
//             {
//                 a: '首页',
//                 b: '/'
//             },
//             {
//                 a: '音乐',
//                 b: '/music'
//             }
//         ]
//     });
// });

// app.get('/music', (req, res) => {
//     res.render('cc.html', {
//         title: '音乐页', content: '我是音乐页', list: [
//             {
//                 a: '关于页',
//                 b: '/about'
//             },
//             {
//                 a: '首页',
//                 b: '/'
//             }
//         ]
//     });
// });

// app.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 2. 提取路由模块,处理函数模块
const express = require('express');
const app = express();
const router = require('./router');

// 配置模板
app.engine('html', require('express-art-template'));
app.set('views', '../views');
app.use(router);


app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});

// 3. mysql版本的管理系统