// 路由标模块

const express = require('express');
// 创建一个路由表
const router = express.Router();
// 引入处理函数
const ctrl = require('./controller');

router.get('/', ctrl.showIndex);

router.get('/about', ctrl.shouAbout);

router.get('/music', ctrl.showMusic);

module.exports = router;