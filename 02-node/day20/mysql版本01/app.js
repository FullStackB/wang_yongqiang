const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// 1.1 配置模板
app.engine('html', require('express-art-template'));
app.set('views', './views');
// 1.2 配置bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
// 2 配置路由
const router = require('./router');
app.use(router);

app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});