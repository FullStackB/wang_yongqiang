// 处理函数模块
// 1.0 配置mysql
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: '127.0.0.1',//地址
    port: '3306',//端口
    user: 'root',
    password: 'root',
    database: 'dany'//数据库名
});

let showIndex = (req, res) => {
    res.render('index.html');
}

let showAdmin = (req, res) => {//get
    // 查询数据表
    connection.query("SELECT * FROM students", (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 渲染管理页
            res.render('show.html', { list: result });
        }
    });
}

let adduser = (req, res) => {//post
    let tt = req.body;
    connection.query('INSERT INTO students(username,password) VALUES ("' + tt.username + '","' + tt.password + '")', (err, result) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

// 对result操作 并没用  
let deluser = (req, res) => {//get
    connection.query('delete from students where id =' + req.query.id, (err, result) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

module.exports = {
    showIndex,
    showAdmin,
    adduser,
    deluser
}