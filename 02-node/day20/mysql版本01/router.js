//  路由模块
const express = require('express');
const router = express.Router();
// 导入 处理函数
const ctrl = require('./controller');

// 显示主页
router.get('/', ctrl.showIndex);
// 显示管理页
router.get('/admin', ctrl.showAdmin);
// 增加用户
router.post('/adduser', ctrl.adduser);
// 删除用户
router.get('/deluser', ctrl.deluser);

module.exports = router;