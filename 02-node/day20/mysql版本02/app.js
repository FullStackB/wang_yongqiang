const express = require('express');
const app = express();
// 配置模板引擎
app.engine('html', require('express-art-template'));
app.set('views', './views');
// 配置bodyparser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// 配置路由
const router = require('./router');
app.use(router);

app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
})