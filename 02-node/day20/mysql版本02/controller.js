// 处理函数模块

// 配置 mysql
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: '127.0.0.1',//地址
    port: '3306',//端口
    user: 'root',
    password: 'root',
    database: 'dany'//数据库名
});


let showIndex = (req, res) => {
    res.render('index.html');
}

let showAdmin = (req, res) => {
    connection.query('select * from users', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.render('show.html', { list: result })
        }
    });
}

let adduser = (req, res) => {
    let d = req.body;
    // connection.query('insert into users(username,password) values("' + d.username + '", "' + d.password + '")', (err) => {
    connection.query("insert into users(username,password) values('" + d.username + "', '" + d.password + "')", (err) => {

        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

let deluser = (req, res) => {
    connection.query('delete from users where id =' + req.query.id, (err, result) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

module.exports = {
    showAdmin,
    showIndex,
    adduser,
    deluser
}
