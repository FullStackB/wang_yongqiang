// 路由模块

const express = require('express');
const router = express.Router();
// 导入 处理函数
const ctrl = require('./controller');

// 首页
router.get('/', ctrl.showIndex);

// 管理页
router.get('/admin', ctrl.showAdmin);

// 添加数据
router.post('/adduser', ctrl.adduser);

// 删除数据
router.get('/deluser', ctrl.deluser);

module.exports = router;