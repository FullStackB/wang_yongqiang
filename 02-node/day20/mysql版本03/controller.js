// 处理函数模块

// 导入mysql
const mysql = require('mysql');
const connction = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'dany'
});

let showIndex = (req, res) => {//get
    res.render('index.html');
}

let showAdmin = (req, res) => {//get
    connction.query('select * from users', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.render('show.html', { list: result });
        }
    });
}

let adduser = (req, res) => {//post
    // console.log(req.body);
    connction.query('insert into users(username,password) values("' + req.body.username + '","' + req.body.password + '")', (err) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

let deluser = (req, res) => {//get
    connction.query('delete from users where id =' + req.query.id, (err) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');

}

module.exports = {
    showAdmin,
    showIndex,
    adduser,
    deluser
}