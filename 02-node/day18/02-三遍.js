// 1.基本web服务器
// 1.1 导入http模块
// const http = require('http');
// // 1.2 创建服务器
// const server = http.createServer();
// // 1.3 给服务器request事件
// server.on('request', (req, res) => {
//     res.writeHead(200, { 'Content-type': 'text/plain;charset=utf-8' });
//     res.end('我的世界开始下雪');
// });
// // 1.4 监听端口
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 2.介绍express框架
// 2.1 express框架为我们开发了很多有用的包 便于开发
// 2.2 express框架是在node原生方法的基础上做了一层封装
// 2.3 express和node 相当于 js和jquery的关系

// 3.使用express创建服务器
// // 3.1 导入express模块
// const express = require('express');
// // 3.2 创建express服务器
// const server = express();
// // 3.3 给服务器一个接口
// server.get('/', (req, res) => {
//     res.send('忽而嘿哟,忽而嘿哟');
// })
// // 3.4 监听端口
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 4.express服务器加载静态资源
// let str = `  
//     <!DOCTYPE html>
//     <html lang="en">

//     <head>
//         <meta charset="UTF-8">
//         <meta name="viewport" content="width=device-width, initial-scale=1.0">
//         <meta http-equiv="X-UA-Compatible" content="ie=edge">
//         <title>Document</title>
//         <link rel="stylesheet" href="./css/main.css">
//     </head>

//     <body>
//         <h1>托管静态资源</h1>
//     </body>

//     </html>`;
// const express = require('express');
// const server = express();
// // 给服务器一个地址 加载静态资源
// server.use(express.static('file'));
// server.get('/', (req, res)=> {
//     res.send(str);
// });
// server.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 5.获取get提交的数据
// 5.1 导入express模块
// const express = require('express');
// const qs = require('querystring');
// // 5.2 创建服务器
// const server = express();
// // 5.2.1 给一个静态资源地址
// server.use(express.static('public'));
// // 5.3 给服务器一个接口
// server.get('/login', (req, res) => {

//     console.log(req.query);
//     res.send(req.query.username + '===' + req.query.password);
// });
// // 5.4 监听端口
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 6.获取posit提交的数据
// 6.1 导入express框架 body-parser包
const express = require('express');
const bodyParser = require('body-parser');
// 6.2 创建服务器
const server = express();
// 6.2.1 给一个静态资源地址
server.use(express.static('public'));
// 6.2.2 初始化 bodyParser包
server.use(bodyParser.urlencoded({ extend: false }));
// 6.3 给一个post接口
server.post('/login', (req, res) => {
    res.send(req.body.username1 + '===' + req.body.password1);
});
// 6.4 监听接口
server.listen(3000, ()=>{
   console.log(' http://127.0.0.1:3000');
});
