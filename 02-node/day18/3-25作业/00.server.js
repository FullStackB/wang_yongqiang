// 引入包 
const express = require('express');
const qs = require('querystring');
const fs = require('fs');

// 1.创建服务器
const server = express();
// 1.1 给一个静态资源目录
server.use(express.static('public'));

// 2.1 login接口
server.get('/login', (req, res) => {
    // 2.1.1 存表单数据
    let sysDate = fs.readFileSync('./public/file/login.json', 'utf-8');
    if (sysDate.length != 0) {
        // 已有数据 json -> 数组 -> push -> json
        let arr = JSON.parse(sysDate);
        arr.push(req.query);
        fs.writeFileSync('./public/file/login.json', JSON.stringify(arr));
    } else {
        // 没有数据 数据-->json
        let temp = [req.query];
        fs.writeFileSync('./public/file/login.json', JSON.stringify(temp));
    }

    // 2.1.2 写入login.html
    res.send(fs.readFileSync('./public/login.html', 'utf-8'));
});

// 2.2 conteng接口
server.get('/content', (req, res) => {
    // json -> 数组
    let temp = fs.readFileSync('./public/file/login.json', 'utf-8');
    let sysDate = JSON.parse(temp);
    // 2.2.0 比对 账号,密码
    for (let i = 0; i < sysDate.length; i++) {
        if (req.query.userName == sysDate[i].userName) {
            if (req.query.passWord == sysDate[i].passWord) {
                res.send(fs.readFileSync('./public/content.html', 'utf-8'));
            }
        }
    }
    res.send('<h1>账号信息有误</h1>');
});

// 3. 启动服务器
server.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});