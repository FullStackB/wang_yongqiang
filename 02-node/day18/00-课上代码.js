// // 引包
// const express = require('express');
// // 创建服务器
// const app = express();
// // 监听地址
// app.get('/', (req, res)=> {
//     res.send('第一个express服务器');
// });
// // 启动服务器
// app.listen(3000, ()=> {
//     console.log('http://127.0.0.1:3000');
// });

// 1.引包
// const express = require('express');
// const app = express();
// app.get('/', (req, res) => {
//     res.send('再来');
// });
// app.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 静态资源 托管静态资源
// 为了释放前端代码 就需要托管静态资源


// const express = require('express');
// // const qs = require('querystring');
// const app = express();
// // app.use(express.static('public'));
// // app.use('/public', express.static('public'));
// app.use(express.static('public'));
// app.get('/', (req, res) => {
//     res.send('首页');
// });

// app.get('/login', (req, res) => {
//     console.log(req.query);
// })
// app.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });


// 导入querystring包 获取提交的数据
// get 提交的数据 req.query对象里存着

// 导入body-Parser包 获取post提交的数据
// server.use(bodyParser.urlencoded({extend:false}));
// post 提交的数据在 req.body对象里存