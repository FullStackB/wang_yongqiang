// 1. 基本的web服务器
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res) => {
//     res.end('a b c d e f g');
// });
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 2. express介绍
// 2.1 express框架便于我们开发
// 2.2 express框架是在node的原生方法上做了一层封装
// 2.3 express和node 相当于js和 jqery的关系

// 3. express的一个简单服务器
// // 3.1 导入express框架
// const express = require('express');
// // 3.2 创建一个express服务器
// const server = express();
// // 3.3 给服务器一个接口
// server.get('/', (req, res) => {
//     res.send('我的世界开始下雪');
// });
// // 3.4 启动服务器
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 4. express.static 托管静态资源
// let str = `  
//     <!DOCTYPE html>
//     <html lang="en">

//     <head>
//         <meta charset="UTF-8">
//         <meta name="viewport" content="width=device-width, initial-scale=1.0">
//         <meta http-equiv="X-UA-Compatible" content="ie=edge">
//         <title>Document</title>
//         <link rel="stylesheet" href="./css/main.css">
//     </head>

//     <body>
//         <h1>托管静态资源</h1>
//     </body>

//     </html>`;
//     const express = require('express');
//     const server = express();
//     // 给服务器一个静态资源地址
//     server.use(express.static('file'));
//     server.get('/', (req, res) => {
//         res.send(str);
//     });
//     server.listen(3000, ()=> {
//         console.log('http://127.0.0.1:3000');
//     });

// 5. 拿到get提交的数据
    // // 5.1 导入express包 querystring包
    //     const express = require('express');
    //     const qs = require('querystring');
    // // 5.2 创建服务器
    //     const server = express();
    // // 5.2.1 给服务器一个静态资源地址
    //     server.use(express.static('public'));
    // // 5.3 给服务器一个接口
    //     server.get('/login', (req, res)=> {
    //         res.send(req.query.username +'===='+req.query.password);
    //     });
    // // 5.4 启动服务器
    //     server.listen(3000, ()=> {
    //         console.log('http://127.0.0.1:3000');
    //     });

// 6. 拿到post提交的数据
    // 6.1 导入express包 body-parser包
        const express = require('express');
        const bodyParser = require('body-parser');
    // 6.2 创建服务器
        const server = express();
    // 6.2.1 给服务器一个静态资源地址
        server.use(express.static('public'));
    // 6.2.2 body-parser包初始化
        server.use(bodyParser.urlencoded({extend: false}));
    // 6.3 给一个post 接口
        server.post('/login', (req, res)=> {
            res.send(req.body.username1 +'====='+ req.body.password1);
        });
    // 6.4 启动服务器
        server.listen(3000, ()=> {
            console.log('http://127.0.0.1:3000');
        });