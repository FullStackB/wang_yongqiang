// 1. 基本的web服务器
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res)=> {
//     res.writeHead(200, {"Content-type": "text/plain; charset=utf-8"});
//     res.end('简单http服务器');
// });
// server.listen(3000, ()=> {
//     console.log('请访问: http://127.0.0.1:3000')
// });

// 2. express介绍
//    2.1 express框架便于我们开发
//    2.2 express框架是在node的原生方法的基础上做了一层封装
//    2.3 express和node 相当于js 和jqery 的关系

// 3. express的一个简单服务器
// 3.1 导入 express包
// const express = require('express');
// // 3.2 创建一个服务器
// const app = express();
// // 3.3 给服务器一个接口
// app.get('/', (req, res) => {
//     res.send('首页');
// });
// // 3.4 启动服务器 监听端口
// app.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 4. express.static 托管静态资源
// let str = `  
//     <!DOCTYPE html>
//     <html lang="en">

//     <head>
//         <meta charset="UTF-8">
//         <meta name="viewport" content="width=device-width, initial-scale=1.0">
//         <meta http-equiv="X-UA-Compatible" content="ie=edge">
//         <title>Document</title>
//         <link rel="stylesheet" href="./css/main.css">
//     </head>

//     <body>
//         <h1>托管静态资源</h1>
//     </body>

//     </html>`;
// // 4.1 导入express包
// const express = require('express');
// // 4.2 创建express服务器
// const server = express();
// // 4.3 给服务器接口
// // 4.3.1 给一个静态资源地址
// server.use(express.static('file'));
// server.get('/', (req, res) => {
//     res.send(str);
// });
// // 4.4 监听端口
// server.listen(3000, () => {
//     console.log('http://127.0.0.1:3000');
// });

// 5. 服务器获取get提交的数据
// const express = require('express');
// const qs = require('querystring');
// const server = express();
// // 给个静态资源地址
// server.use(express.static('public'));
// server.get('/login', (req, res) => {
//     console.log(req.query);
//     res.end('cc');
// });
// server.listen(3000, () => {
//     console.log('http:127.0.0.1:3000');
// });

// 6. 服务器获取post提交的数据
// 6.1 引入express模块
// const express = require('express');
// const bodyParser = require('body-parser');
// // 6.2 创建express服务器
// const server = express();
// server.use(bodyParser.urlencoded({ extended: false }));
// // 6.2.1 给个静态资源地址
// server.use(express.static('public'));
// // 6.3 给一个接口
// server.post('/login', (req, res) => {
//     console.log(req.body.username1, req.body.password1);
// });
// // 6.4 监听端口
// server.listen(3000, () => {
//     console.log('http:127.0.0.1:3000');
// });

const express = require('express');

const cc = require('body-parser');
const server = express();

server.use(express.static('public'));
server.use(cc.urlencoded({ extended: false }));

server.post('/login', (req, res) => {
    res.send(req.body.username1 + '-----' + req.body.password1);
})

server.listen(3000, () => {
    console.log("请访问:http://127.0.0.1:3000")
})