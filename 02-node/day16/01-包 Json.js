// 1. node.js 模块: 核心模块 第三方模块 自定义模块
//  随着node.js的安装包 一同安装到本地的模块 核心模块

// 2. 包 英文名叫做Packages, 包是在模块基础上更深一步的抽象
//    目的: 方便分发和推广基于CommonJs规范实现的 应用程序 或类库
//    包可以看作 模块.代码和其他资源组合起来形成的独立作用域
//    包就是一个文件夹 文件夹中所有文件 都是为这个包提供功能服务的 

// 3.包的规范
// 3.1 包都要以一个单独的目录存在
// 3.2 package.json 必须在报的顶层目录下
// 3.3 package.json 文件必须符合JSON格式,包含三个属性(name, version, main)
//     name 包的名字,version 包的版本号,main 包的入口文件

// 4.json
// 为什么需要json 后端给前端的数据大部分是json
// json 是符合 javascript 对象或数组格式的字符串
// json数据转数组(对象)  数组(对象)转json

// let jsonArr = "[1,2,3,4,5]"; //json 类型数组
// let jsonObj = '{"name":"王永强","age":20}';
// // let result = JSON.parse(jsonArr);
// // console.log(Array.isArray(result));
// let result = JSON.parse(jsonObj);
// console.log(result instanceof object);

// 数组,对象转json字符串 
let arr = [1, 1, 1, 1, 1];
let jsonArr = JSON.stringify(arr);
let obj = { name: '王永强', age: "23" };
let jsonObj = JSON.stringify(obj);
console.log(jsonArr);
console.log(jsonObj);