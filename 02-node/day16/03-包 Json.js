// 1 node.js 模块 核心模块 第三方模块 自定义模块
// 2. 包 packages 包是魔魁哎基础上更深一步的抽象
// 目的： 方便分发和推广 基于CommonJs规范实现的应用程序 或类库
// 包可以看作模块 代码 和其他资源组合形成的独立作用域
// 包是一个文件夹 文件夹中所有文件都是为这个包提供服务的

// 3. 包的规范 
// 包都要以一个独立的文件夹存在
// package.json 必须在包的顶级目录下
// package.json 文件必须符合JSON 格式 包含三个属性(name version main)

// 4.json 
// 为什么需要json 后端给前端的数据大部分是json
// json 是符合 javascript 兑现干活i数组格式的字符串
// let jsonArr = '[1,234,56,7]';
// let jsonObj = `{
//     "name": "wyq",
//     "age": "12",
//     "sex": "男"
// }`;
// console.log(JSON.parse(jsonArr));
// console.log(JSON.parse(jsonObj));

let arr = [123145, 546, 123];
let obj = {
    name: '迟天宇',
    age: '12',
    sex: 'man'
};
console.log(JSON.stringify(arr));
console.log(JSON.stringify(obj));