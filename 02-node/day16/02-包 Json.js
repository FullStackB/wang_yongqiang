// 1 node.js 模块 核心模块 第三方模块 自定义模块

// 2. 包 英文名 叫做Packages， 包是模块基础上更深一步的抽象
// 目的: 方便分发和推广 基于CommonJs规范实现的应用程序 或类库
// 包可以看作 模块 代码和其他资源组合形成的独立作用域
// 包是一个文件夹 文件夹中所有文件都是为这个包提供功能服务的

// 3.报的规范
// 包都要以一个单独的文件夹存在
// package.json 必须在包的顶级目录下
// package.json 文件必须符合JSON格式,包含三个属性 (name, version, main)

// 4.json
// 为什么需要json 后端给前端的数据大部分是json
// json 是符合 javascript 对象或数组格式的字符串
// json 数据转对象(数组) 数组(对象)转json
// let jsonArr = '[1,2,3,4,5,6]';
// let jsonObj = `{
//     "name": "王永强",
//     "age": 23,
//     "sex": "男"
// }`;
// console.log(JSON.parse(jsonArr));
// console.log(JSON.parse(jsonObj));

let arr = [1,1,23,4,5];
let obj = {
    name: '王永强',
    sex: '男',
    age: '20'
};
console.log(JSON.stringify(arr));
console.log(JSON.stringify(obj));
