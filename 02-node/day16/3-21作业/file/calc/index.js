// 引包
const add = require('./lib/add');
const chen = require('./lib/chen');
const chu = require('./lib/chu');
const qy = require('./lib/qy');
const minus = require('./lib/minus');

// 暴露运算函数
module.exports = {
    add,
    chen,
    chu,
    minus,
    qy
};