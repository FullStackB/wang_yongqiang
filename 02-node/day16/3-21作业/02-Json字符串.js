const fs = require('fs');
const path = require('path');

// 1. 获取json文件内容
var str = fs.readFileSync(path.join(__dirname, './file/package-lock.json'), 'utf-8');
console.log(str);
// 2. Json字符串转对象
var obj = JSON.parse(str);
console.log(obj);
// 3. 对象转JSon字符 写入文件
var result = JSON.stringify(obj);
fs.writeFileSync(path.join(__dirname, './file/newJson.txt'), result);