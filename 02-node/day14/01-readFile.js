// 导入 fs模块
const fs = require('fs');
// 调用 readFile方法
fs.readFile(__dirname + '/files/01-read.txt', 'utf8', (err, date) => {
    if (err) return console.log('读取文件失败: ' + err.message);
    console.log(date);
});