// 导入 fs模块
const fs = require('fs');

// 1.写入内容
let str = '我是传智专修学院的大学生，我要好好学习';
fs.writeFile(__dirname + '/files/03-write.txt', str, err => {
    if (err) {
        console.log(err.message);
    }
});

// 2.追加内容
fs.appendFile(__dirname + '/files/03-write.txt', '\n我是最棒的', err => {
    if (err) {
        console.log(err.message);
    }
});

// 3.读取内容
fs.readFile(__dirname + '/files/03-write.txt', 'utf8', (err, date) => {
    if (err) {
        console.log(err.message);
    } else {
        console.log(date);
    }
});