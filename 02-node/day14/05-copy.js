// 导入 fs模块
const fs = require('fs');
let flag = false;

// 1.写入内容
fs.writeFile(__dirname + '/files/05-copy.txt', '我准备拷贝文件', (err) => {
    if (err) console.log('写入内容失败' + err.message);
});

// 2.追加内容
fs.appendFile(__dirname + '/files/05-copy.txt', '\n我继续追加信息到文件中', err => {
    if (err) console.log('追加内容失败' + err.message);
    flag = true;
});

// 3.获取信息
fs.stat(__dirname + '/files/05-copy.txt', (err, stats) => {
    if (err) return console.log('获取信息失败' + err.message);

    // 文件大小
    console.log(stats.size);
    // 文件创建时间
    console.log(stats.birthtime);
    // 判断是不是文件
    console.log(stats.isFile());
    // 判断是不是目录
    console.log(stats.isDirectory());

    // 4.复制文件
    fs.copyFile(__dirname + '/files/05-copy.txt', __dirname + '/files/05-copyFile.txt', err => {
        if (err) console.log('复制文件失败' + err.message);
    });
});
