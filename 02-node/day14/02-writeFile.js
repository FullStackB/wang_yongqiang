// 导入fs模块
const fs = require('fs');

// 1.写入内容
let str = '我是传智专修学院的大学生，我要好好学习';
fs.writeFile(__dirname + '/files/02-write.txt', str, (err) => {
    if (err) return console.log(err.message);
    console.log('成功写入内容!');
});

// 2.读取内容
fs.readFile(__dirname + '/files/02-write.txt', 'utf8', (err, date) => {
    if (err) {
        console.log(err.message);
    } else {
        console.log(date);
    }
});