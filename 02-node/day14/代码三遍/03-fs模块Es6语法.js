// 1. let const 不会提升变量
// console.log(a);
// let a = 'cc';
//    let const 有块级作用域
// {
//     let a = 'cc';
// };
// console.log(a);
//    let const 不能在同一作用域内重复声明
// let a = 'wer';
// let a = 'acaf';
// console.log(a);
//    const 不能再次赋值
// const a = 'dd';
// a = 'afa';
// console.log(a);

// 2. 块级作用域 可避免内层变量覆盖外层变量
//              可避免计数的循环变量泄露为全局变量

// 3.解构赋值
// let obj = {
//     name: '王永强',
//     sex: '男',
//     age: '20',
//     sayHi: function () {
//         console.log('我的世界开始下雪');
//     }
// };
// let { name, sex, age } = obj;
// console.log(name, sex, age);

// 4. 箭头函数 就是普通函数的简写(箭头函数中没有this)
// let fn = (str) => {
//     console.log(str);
// }
// fn('完蛋了吧');

// 5. 对象中属性,方法的间歇方式
// let name = 'cc';
// let age = '泰勒';
// let sex = 'man';
// let obj = {
//     name,
//     age,
//     sex,
//     sayHi() {
//         console.log(this.name, this.age, this.sex);
//     }
// };
// obj.sayHi();

// 6.文件操作
// <1 读取文件
// const fs = require('fs');
// fs.readFile(__dirname+ '/file/2.txt', 'utf-8', (err, date)=>{
//     if (err) return console.log(err.message);
//     console.log(date);
// });
// <2 写入文件
// const fs = require('fs');
// fs.writeFile(__dirname + '/file/2.txt', '随便写点东西', (err) => {
//     if (err) return console.log(err.message);
// });
// <3 追加文件
// const fs = require('fs');
// fs.appendFile(__dirname + '/file/2.txt', '\n我是添加的内容', err => {
//     if (err) return console.log(err.message);
// });
// <4 复制文件
// const fs = require('fs');
// fs.copyFile(__dirname + '/file/2.txt', __dirname + 'file/copy-3.txt', err => {
//     if (err) {
//         return console.log(err.message);
//     }
// });
// <5 文件信息
// const fs = require('fs');
// fs.stat(__dirname + '/file/2.txt', (err, stats) => {
//     if (err) {
//         console.log(err.message)
//     };

//     // 文件大小
//     console.log(stats.size);
//     // 文件创建时间
//     console.log(stats.birthtime.toString());
//     // 判断是不是一个文件
//     console.log(stats.isFile());
//     // 判断是不是一个文件夹
//     console.log(stats.isDirectory());
// });
// <6 读取文件夹中所有文件名
const fs = require('fs');
fs.readdir(__dirname, (err, files) => {
    if (err) return console.log(err.message);
    console.log(files);
});