// 1. ==== let const
// 不会提升变量
// console.log(a);
// let a = 'cc';
// 有块级作用域
// {
//     let cc = '我的世界开始下雪';
// };
// console.log(cc);
// 在同一作用域内不能重复声明
// let a = 'cc';
// let a = 'bb';
// console.log(a);
// const 不能再次赋值
// const a = 'cc';
// a = '我的世界开始下雪';
// console.log(a);

// 2. ==== 块级作用域
// 可避免内层变量覆盖外层变量
// 可避免用来计数的循环变量泄露

// 3. ==== 解构赋值 把对象中属性和方法拿出来的简便方法
// var obj = {
//     name: '王永强',
//     sex: '男',
//     age: '23'
// };
// var { name, sex, age } = obj;
// console.log(name, sex, age);

// 4. ==== 箭头函数 就是匿名函数的简写
// let fn = (x, y) => {
//     console.log(x + y);
// }
// fn(1, 5);

// 5. 对象中属性方法的简写方式
// let name = '王永强';
// let age = '20';
// let sex = '男';
// let obj = {
//     name,
//     age,
//     sex,
//     sayHi() {
//         console.log('是福不是祸,是祸躲不过');
//     }
// }
// console.log(obj);

// 6. ==== 文件操作
// <1 读取文件
// const fs = require('fs');
// fs.readFile(__dirname + '/file/2.txt', 'utf8', (err, date) => {
//     if (err) return console.log(err.message);
//     console.log('内容:' + date);
// });
// <2 写入文件
// const fs = require('fs');
// fs.writeFile(__dirname + '/file/2.txt', '内容会覆盖', (err) => {
//     if (err) return console.log('写入内容失败!');
// });
// <3 追加文件
// const fs = require('fs');
// fs.appendFile(__dirname + '/file/2.txt', '\n换行插入', err => {
//     if (err) return console.log(err.message);
// });
// <4 复制文件
// const fs = require('fs');
// fs.copyFile(__dirname + '/file/copy.txt', __dirname + '/file/copy1.txt', err => {
//     if (err) return console.log(err.message);
// });
// <5 读取文件信息
// const fs = require('fs');
// fs.stat(__dirname + '/file/2.txt', (err, stats) => {
//     if (err) return console.log(err.message);

//     // 文件大小
//     console.log(stats.size);
//     // 文件创建时间
//     console.log(stats.birthtime.toString());
//     // 判断是不是一个文件
//     console.log(stats.isFile());
//     // 判断是不是一个文件夹
//     console.log(stats.isDirectory());
// });
// <6 读取指定目录下所有文件名
const fs = require('fs');
fs.readdir(__dirname, (err, files) => {
    if (err) return console.log(err);
    console.log(files);
});