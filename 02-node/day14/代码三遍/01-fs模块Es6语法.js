// 1. ===== let const
//let const 有块级作用域
//     {
//         let a = 'cc';
//     }
//     console.log(a);// a is not defined

//let const 不允许在相同作用域内重复声明一个变量
//     let a = 'cc';
//     let a = 'bb';
//     console.log(a);//Identifier 'a' has already been declared

//let const 不存在变量提升
//     console.log(a);
//     let a = 'a';//a is not defined

//const 不允许再次赋值
//     const a = 12;
//     a = 23;
//     console.log(a);// Assignment to constant variable.

// 2. ==== 块级作用域
//  可避免内层变量覆盖外层变量
//  可避免用来计数的循环变量泄露为全局变量

// 3. ==== 解构赋值便捷的拿出对象中的属性,方法
// var ob = {
//     name: '王永强',
//     age: '20',
//     sex: '男',
//     height: '170cm',
//     weight: '130斤'
// };

// var { name, age, height, weight, sex } = ob;
// console.log(name, age, sex, height, weight);

//  4. ==== 箭头函数 匿名函数的简写
// () => {}
// let fn = (x, y) => {
//     console.log(x + y);
// }
// fn(1, 1);

// 5. === 对象中属性和方法简写
// let name = 'cc';
// let age = '20';
// let weight = '170cm';
// let height = '60kg';
// let obj = {
//     name,
//     age,
//     weight,
//     height,
//     sayHi() {
//         console.log('我的世界开始下雪');
//     }
// };
// console.log(obj);

// 6.文件操作
//  读取文件
// const fs = require('fs');
// fs.readFile('./file/c.txt', 'utf8', (err, date)=>{
//     if (err) return console.log('读取文件失败!');
//     console.log('读取文件成功!');
//     console.log(date);
// });
//  写入文件
// const fs = require('fs');
// fs.writeFile('./file/2.txt', 'writeFile有三个参数 一个文件保存地址 一个写入数据 一个匿名函数', err => {
//     if (err) return console.log(err.message);
//     console.log('文件写入成功!'); 
// });
//  追加文件
// const fs = require('fs');
// fs.appendFile('./file/2.txt', '\n责任担当自信', err => {
//     if (err) return console.log(err.message);
//     console.log('文件追加成功');
// });
//  复制文件
// const fs = require('fs');
// fs.copyFile('./file/c.txt', './file/copy.txt', err => {
//     if (err) return console.log(err.message);
//     console.log('复制文件成功!');
// });
// 读取文件信息
// const fs = require('fs');
// fs.stat(__dirname + '/file/2.txt', (err, stats) => {
//     if (err) return console.log(err);

//     // 文件大小
//     console.log(stats.size);
//     // 文件创建时间
//     console.log(stats.birthtime.toString());
//     // 判断是不是文件
//     console.log(stats.isFile());
//     // 判断是不是文件夹
//     console.log(stats.isDirectory());
// });
// 读取指定目录下所有文件名
const fs = require('fs');
fs.readdir(__dirname, 'utf8', (err, files) => {
    if (err) return console.log('读取文件名失败!');
    console.log(files);
});