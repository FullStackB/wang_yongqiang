// 导入 fs模块
const fs = require('fs');

// 1.写入内容
fs.writeFile(__dirname + '/files/04-stat.txt', '全栈谁最牛', err => {
    if (err) console.log('写入内容失败!' + err.message);
});

// 2.追加内容
fs.appendFile(__dirname + '/files/04-stat.txt', '\n我最牛', err => {
    if (err) console.log('追加内容失败!' + err.message);
});

// 3.获取文件信息
fs.stat(__dirname + '/files/04-stat.txt', (err, stats) => {
    if (err) return console.log('获取信息失败!' + err.message);

    // 获取文件大小
    console.log(stats.size);
    // 获取文件创建时间
    console.log(stats.birthtime);
    // 判断是不是一个文件
    console.log(stats.isFile());
    // 判断是不是目录
    console.log(stats.isDirectory());
});

