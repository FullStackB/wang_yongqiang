// 处理函数模块

// 导入mysql
const mysql = require('mysql');
// 建立连接
const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'dany'
});

let showIndex = (req, res) => {
    res.render('index.html');

};

let showAdmin = (req, res) => {
    connection.query('select * from user', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.render('show.html', { list: result });
        }
    });
};

// let id;
let showGai = (req, res) => {
    // id = req.query.id;
    res.render('gai.html', { id: req.query.id });
}

let addUser = (req, res) => {//get
    connection.query('insert into user(name,password) values("' + req.body.username + '","' + req.body.password + '")', (err) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
};

let delUser = (req, res) => {//get
    connection.query('delete from user where id =' + req.query.id, (err) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

let upData = (req, res) => {//post
    connection.query('update user set name ="' + req.body.username + '",password = "' + req.body.password + '"' + 'where id =' + req.body.id, (err) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

module.exports = {
    showIndex,
    showAdmin,
    showGai,
    addUser,
    delUser,
    upData
}