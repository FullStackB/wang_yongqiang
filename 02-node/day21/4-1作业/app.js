const express = require('express');
const bodyParser = require('body-parser');
const router = require('./router');
const app = express();

// 配置模板
app.engine('html', require('express-art-template'));
app.set('views', './views');
// 配置bodyParser
app.use(bodyParser.urlencoded({ extended: false }));

// 配置路由
app.use(router);
app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});
