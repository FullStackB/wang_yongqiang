// 路由模块
const express = require('express');
const router = express.Router();
// 导入处理函数模块
const ctrl = require('../controller');

// 显示注册页
router.get('/', ctrl.showIndex);
// 显示管理页
router.get('/admin', ctrl.showAdmin);
// 显示修改页
router.get('/gai', ctrl.showGai);

// 增加数据
router.post('/adduser', ctrl.addUser);
// 删除数据
router.get('/deluser', ctrl.delUser);
// 修改数据
router.post('/update', ctrl.upData);

module.exports = router;

