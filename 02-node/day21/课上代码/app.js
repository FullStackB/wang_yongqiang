const express = require('express');
const app = express();

// 设置静态资源目录
app.use('/public', express.static('public'));

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));

app.get('/', (req, res) => {
    res.sendFile('./views/post.html', { root: __dirname });
});


app.get('/adduser', (req, res) => {
    console.log(req.query);
    res.send(req.query);
});


app.post('/adduser', (req, res) => {
    res.send(req.body);
});


app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});