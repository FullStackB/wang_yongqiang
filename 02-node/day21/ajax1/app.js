const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// 设置静态目录
app.use('/public', express.static('public'));
// 设置bodyParser
app.use(bodyParser.urlencoded({ extended: false }));


app.get('/', (req, res) => {
    // res.sendFile('./views/get.html', { root: __dirname });
    res.sendFile('./views/post.html', { root: __dirname });
});

app.get('/adduser', (req, res) => {
    // res.send(req.query.username + ' == ' + req.query.password);
    console.log(req.query);
    res.send(req.query);
});

app.post('/adduser', (req, res) => {
    // res.send(req.body.username + '===' + req.query.password);
    res.send(req.body);
});

app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});