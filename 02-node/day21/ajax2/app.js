const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// 设置bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
// 设置静态资源地址
app.use('/public', express.static('public'));

app.get('/', (req, res) => {
    // res.sendFile('./views/get.html', { root: __dirname });
    res.sendFile('./views/post.html', { root: __dirname })
});
// get 注册
app.get('/adduser', (req, res) => {
    // console.log(req.query);
    res.send(req.query.username + '===' + req.query.password);
});
// post 注册
app.post('/adduser', (req, res) => {
    // console.log(req.body);
    res.send(req.body);
});

app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});