const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// 配置静态资源
app.use('/public', express.static('public'));


app.get('/', (req, res) => {
    // 开始显示 get提交页
    // res.sendFile('./views/get.html', { root: __dirname });

    // 开始显示 post提交页
    res.sendFile('./views/post.html', { root: __dirname });
});

// 处理get提交
app.get('/adduser', (req, res) => {
    res.send(req.query);
});

// 处理post提交
app.post('/adduser', (req, res) => {
    res.send(req.body);
});

app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});