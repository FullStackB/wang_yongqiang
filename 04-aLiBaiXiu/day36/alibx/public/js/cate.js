// #region 显示所有分类
let show = function () {
    $.ajax({
        type: 'get',
        url: '/catesFind',
        data: '',
        success: function (result) {
            let content = template('cate_show_template', {
                list: result
            });
            $('tbody').html(content);
        }
    });
}
show();
// #endregion

// #region 增加分类
$('.add-btn').on('click', function () {
    $.ajax({
        type: 'post',
        url: '/cateAdd',
        data: $('.add-form').serialize(),
        success: function (result) {
            location.reload();
        }
    });
});
// #endregion

// #region 删除单个分类
$('tbody').on('click', '.del-btn', function () {
    $.ajax({
        type: 'get',
        url: '/cateDelete',
        data: {
            id: $(this).data('id')
        },
        success: function (result) {
            show();
        }
    });
});
// #endregion

// #region 批量删除
$('.checkall').on('click', function () {
    let flag = $(this).prop('checked');
    $('.checkitem').prop('checked', flag);

    if (flag) $('.del-more').show('fast');
    else $('.del-more').hide();
});

$('tbody').on('click', '.checkitem', function () {
    let sum = $('.checkitem').size();
    let num = $('.checkitem:checked').size();

    if (sum == num) $('.checkall').prop('checked', true);
    else $('.checkall').prop('checked', false);

    if (num > 1) $('.del-more').show('fast');
    else $('.del-more').hide();
});

$('.del-more').on('click', function () {
    let idArr = [];
    $('.checkitem:checked').each(function (index, ele) {
        idArr.push($(ele).data('id'));
    });
    console.log(idArr);
    $.ajax({
        type: 'get',
        url: '/catesDelete',
        data: {
            idArr: idArr
        },
        success: function (result) {
            show();
        }
    })
});
// #endregion

// #region 获取一条分类
$('tbody').on('click', '.exit-btn', function () {
    $.ajax({
        type: 'get',
        url: '/cateFind',
        data: {
            id: $(this).data('id')
        },
        success: function (result) {
            // console.log(result);
            let content = template('cate_exit_template', result);
            $('.col-md-4').html(content);
        }
    })
});
// #endregion

// #region 更新数据
$('.col-md-4').on('click', '.update-btn', function () {
    // console.log($('.col-md-4 .exit-form').serialize());
    $.ajax({
        type: 'post',
        url: '/cateUpdate',
        data: $('.col-md-4 .exit-form').serialize(),
        success: function (result) {
            location.reload();
        }
    });
});
// #endregion