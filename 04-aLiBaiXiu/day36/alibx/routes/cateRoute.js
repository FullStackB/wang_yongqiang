// 分类路由模块
const express = require('express');
const router = express.Router();
const cateCtrl = require('../controllers/cateCtrl');

// 显示分类页
router.get('/cate', cateCtrl.cateShow);

// 获取全部分类
router.get('/catesFind', cateCtrl.catesFind);

// 增加分类
router.post('/cateAdd', cateCtrl.cateAdd);

// 删除单个分类
router.get('/cateDelete', cateCtrl.cateDelete);

// 批量删除
router.get('/catesDelete', cateCtrl.catesDelete);

// 获取一条分类
router.get('/cateFind', cateCtrl.cateFind);

// 更新分类
router.post('/cateUpdate', cateCtrl.cateUpdate);

module.exports = router;