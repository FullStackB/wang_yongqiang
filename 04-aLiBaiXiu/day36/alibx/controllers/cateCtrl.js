const conn = require('../data/index');

// #region 显示首页
module.exports.cateShow = (req, res) => {
    res.render('categories');
}
// #endregion

// #region 获取全部分类
module.exports.catesFind = (req, res) => {
    conn.query('select * from ali_cate', (err, result) => {
        if (err) return console.log(err);

        res.json(result);
    });
}
// #endregion

// #region 添加分类
module.exports.cateAdd = (req, res) => {
    let arr = [req.body.name, req.body.slug];
    conn.query('insert into ali_cate values(null,?,?)', arr, (err, result) => {
        if (err) return console.log(err);

        res.send('ok');
    });
}
// #endregion

// #region 删除单个分类
module.exports.cateDelete = (req, res) => {
    // console.log(req.query.id);
    conn.query('delete from ali_cate where cate_id=' + req.query.id, (err, result) => {
        if (err) return console.log(err);

        res.send('ok');
    });
}
// #endregion

// #region 批量删除
module.exports.catesDelete = (req, res) => {
    // console.log(req.query);
    conn.query('delete from ali_cate where cate_id in(?)', [req.query.idArr], (err, result) => {
        if (err) return console.log(err);

        res.send('ok');
    });
}
// #endregion

// #region 获取一条分类
module.exports.cateFind = (req, res) => {
    conn.query('select * from ali_cate where cate_id=' + req.query.id, (err, result) => {
        if (err) return console.log(err);

        res.json(result[0]);
    });
}
// #endregion

// #region 更新数据
module.exports.cateUpdate = (req, res) => {
    let arr = [req.body.name, req.body.slug, req.body.id];
    // console.log(arr);
    conn.query('update ali_cate set cate_name=?,cate_slug=? where cate_id=?', arr, (err, result) => {
        if (err) return console.log(err);

        res.send('ok');
    });
}
// #endregion