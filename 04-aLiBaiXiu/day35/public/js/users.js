// 功能一: 显示用户列表
// crud ： 增删改查
let showUsers = function () {
  $.ajax({
    type: 'get',
    url: '/usersFind',
    data: '',
    success: function (data) {
      // console.log(data);
      let usersStr = template('users_template', {
        list: data
      });
      $('tbody').html(usersStr);
    }
  })
}
showUsers();
// form标签的id,意思是为这个form标签里面的所有input添加校验规则
// 功能二: 添加用户
$('#users-add')
  .bootstrapValidator({
    message: 'This value is not valid',
    // 反馈图标
    feedbackIcons: {
      // 校验通过的图标
      valid: 'glyphicon glyphicon-ok',
      // 校验不通过的图标
      invalid: 'glyphicon glyphicon-remove',
      // 校验中的图标
      validating: 'glyphicon glyphicon-refresh'
    },
    // fields 字段
    // 里面的username email password 都是input的name属性的值
    fields: {
      email: {
        validators: {
          notEmpty: {
            message: '您的邮箱不能为空'
          },
          stringLength: {
            min: 6,
            max: 25,
            message: '您的邮箱必须是长度为6到25个字符之间'
          },

          regexp: {
            regexp: /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
            message: '您的邮箱的格式不正确'
          }
        }
      },
      slug: {
        validators: {
          notEmpty: {
            message: '您的别名不能为空'
          },
          stringLength: {
            min: 1,
            max: 24,
            message: '您的别名可以是长度为1到24个字符之间'
          },
        }
      },
      nickname: {
        validators: {
          notEmpty: {
            message: '您的昵称不能为空'
          },
          stringLength: {
            min: 1,
            max: 24,
            message: '您的昵称可以是长度为1到24个字符之间'
          }

        }
      },
      password: {
        validators: {
          notEmpty: {
            message: '您的密码不能为空'
          },
          stringLength: {
            min: 4,
            max: 16,
            message: '密码的长度在6-20个字符之间'
          },
          regexp: {
            regexp: /^[a-zA-Z0-9_-]{4,16}$/,
            message: '密码由数字 大小字母下划线组成'
          }
        }
      },
    }
  })
  // 当点击type=submit的按钮的时候 如果校验成功 那么就可以提交ajax请求
  .on('success.form.bv', function (e) {
    // Prevent form submission
    e.preventDefault(); // 因为 type=submit会自动提交 为了阻止自动提交 写了这一个方法

    // Get the form instance(实例  对象 )
    var $form = $(e.target); // document.querySelect('form');
    console.log($form);
    // Get the BootstrapValidator instance
    var bv = $form.data('bootstrapValidator'); // 获取了校验对象

    // Use Ajax to submit form data
    $.ajax({
      type: 'post',
      url: '/userAdd',
      data: $form.serialize(),
      success: function (data) {
        // console.log(data);
        if (data.code == '1000') {
          // location.href = "/users";
          $("#users-add").data('bootstrapValidator').resetForm(); //清除当前验证
          showUsers();
        }
      }
    })
  });

// 功能三: 删除用户
$('table').on('click', '.delete-user', function () {
  // 1.获取点击的按钮身上的id、
  let id = $(this).data('id');
  // 2.发起ajax请求
  $.ajax({
    type: 'get',
    url: '/userDelete',
    data: {
      id: id
    },
    beforeSend: function () {
      let flag = confirm('您确定要删除改用户吗？');
      if (!flag) {
        return false;
      }
    },
    success: function (data) {
      // console.log(data);
      if (data.code == '1001') {
        showUsers();
      }
    }
  })
})

// 根据id查询数据
$('tbody').on('click', '.edit-user', function () {
  $.ajax({
    type: 'get',
    url: '/userFind',
    data: {
      id: $(this).data('id')
    },
    success: function (result) {
      let content = template('update_template', result[0]);
      $('.col-md-4').html(content);
    }
  });
})

// 更新数据
$('.col-md-4').on('click', '.update-btn', function () {
  let pushData = $('#users-add').serialize() + '&id=' + $(this).data('id');
  $.ajax({
    type: 'post',
    url: '/userUpdate',
    data: pushData,
    success: function (result) {
      if (result.code == '1003')
        location.href = '/users';
    }
  });
});

// 批量删除
$('.checkall').on('click', function () {
  let flag = $(this).prop('checked');
  // 全选
  $('tbody .checkitem').prop('checked', flag);
  // 批量删除按钮
  if (flag) $('.del-more').show('fast');
  else $('.del-more').hide();
});

$('tbody').on('click', '.checkitem', function () {
  let target = $('.checkitem').size();
  let now = $('.checkitem:checked').size();
  // 全选后选中全选框
  if (now == target) $('.checkall').prop('checked', true);
  else $('.checkall').prop('checked', false);
  // 批量删除按钮
  if (now > 1) $('.del-more').show('fast');
  else $('.del-more').hide();
});

$('.del-more').on('click', function () {
  let arr = [];
  $('.checkitem:checked').each(function (index, ele) {
    arr.push($(ele).data('id'));
  });
  $.ajax({
    type: 'post',
    url: '/usersDelete',
    data: {ids: arr},
    success: function (result) {
      if (result.code == '1003')
        location.href = '/users';
    }
  });
});