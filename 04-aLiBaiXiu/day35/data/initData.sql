-- 1.创建数据库
 create database alibx;
-- 2.使用数据库
use alibx;
-- 3.创建表
create table users(
  id int primary key auto_increment comment '用户ID',
  avatar varchar(255) default '/uploads/avatar_1.jpg',
  email varchar(25) not null comment '用户邮箱',
  slug varchar(25) not null  comment '用户别名',
  nickname varchar(25) not null comment '用户昵称',
  password varchar(20) not null comment '用户密码',
  status varchar(20) not null default 'activated' comment '未激活（unactivated）/ 激活（activated）/ 禁止（forbidden）/ 回收站（trashed）'

);
set names gbk;
-- 4.插入数据
insert into users values
(null, default, 'xiaoming@163.com', '明明','鸣人','123456','activited'),
(null, default, 'xiaohuang@163.com', '二狗子','大黄','123456','activited'),
(null, default, 'xiaohong@163.com', '小红吧','红蜘蛛','123456','activited'),
(null, default, 'xiaosun@163.com', '孙悟空','悟空','123456','activited'),
(null, default, 'xiaowang@163.com', '王大锤','大锤','123456','activited');