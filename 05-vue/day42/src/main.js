import Vue from 'vue';

// 引入并配置elementUi(全局使用elementUi)
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 全局使用axios
import axios from 'axios';
Vue.prototype.$axios = axios;

import App from './App.vue';

import router from './router';

const vm = new Vue({
  el: '#app',
  router,
  render: h => h(App)
});