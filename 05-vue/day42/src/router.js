import Vue from 'vue';
// 引入路由包并配置
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 引入组件
import home from './views/Home.vue';

// 写路由
const router = new VueRouter({
  routes: [{
      path: '/',
      redirect: '/index'
    },
    {
      path: '/index',
      component: home
    }
  ]
});


export default router;