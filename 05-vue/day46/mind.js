// #region 左侧导航
/**
 *  1. submean > template + meanitem
 *  2. 父路由重定向写在 children项中
 *  3. el-row(gutter列间隔) > el-col(span列长度)
 *  4. axios的get提价传对象需要 params项
 *  5. 外套template ct.row是数组中每个元素
 *  6. v-if v-else-if 据值选择性渲染
 *  7. ref是加给Form表单的
 */
// #endregion

// 对象赋值给变量 变量存的是地址 它俩一起变
let obj = {
    name: '汪峰',
    age: '123'
}

let a = obj;
a.age = '2342';
console.log(obj.age);