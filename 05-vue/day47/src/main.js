import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// 配置element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import './assets/css/base.css';

// 配置axios
import Axios from 'axios';
Axios.defaults.baseURL = "https://www.liulongbin.top:8888/api/private/v1/"; //默认地址

// axios拦截器
Axios.interceptors.request.use(function (config) {
  // 在发起请求请做一些业务处理
  config.headers.Authorization = window.sessionStorage.getItem("token");
  return config;
}, function (error) {
  // 对请求失败做处理
  return Promise.reject(error);
});

Vue.prototype.$http = Axios;


Vue.config.productionTip = false;


new Vue({
  router,
  render: h => h(App)
}).$mount("#app");