const express = require('express');

const app = express();

app.use(express.static('public'));

app.listen(8000, () => {
    console.log('服务器运行中...');
});

/**
 *  0. 现在的vue项目部署后台
 *  1. 执行 npm run build 命令 会生成一个dist文件
 *  2. 写个node.js服务器 把dist文件里的东西 放到public目录下
 *  3. node app.js 即可打开项目 
 */