import Vue from '../lib/vue';

// 引入路由文件并配置
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 引入多个模板
import app from './app.vue';
import index from './index.vue';
import about from './about.vue';
import movie from './movie.vue';
// import inner from './tabs/inner.vue';
// import outer from './tabs/outer.vue';

// 3.路由传参
import movieTarget from './movieTarget.vue';
// 3. 路由传递参数(命名路由) 接收参数(props数组)

// 路由规则
const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            component: index
        },
        {
            path: '/about',
            component: about
        },
        {
            path: '/movie',
            component: movie,
            // redirect: '/movie/inner',
            // children: [ //子路由
            //     {path: '/movie/inner', component: inner },
            //     {path: '/movie/outer', component: outer}
            // ]
        },
        {
            // 命名路由
            name: 'nn',
            path: '/movieTarget/:id',
            component: movieTarget,
            props: true
        }
    ],
    linkActiveClass: 'active' //更改默认高亮类
});


const vm = new Vue({
    el: '#app',
    router,
    render: h => h(app)
});