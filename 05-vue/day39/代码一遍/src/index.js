// #region 前端路由测试
/**
 * import Vue from '../lib/vue';
// 导入并配置 前端路由包
import VueRouter from 'vue-router';
Vue.use(VueRouter);


import App from '../components/App.vue';
import index from '../components/index.vue';
import about from '../components/about.vue';
import movie from '../components/movie.vue';

// 写路由规则
const router = new VueRouter({
    routes: [
        {path: '/', redirect:'/index'},
        {path: '/index', component: index},
        {path: '/about', component: about},
        {path: '/movie', component: movie}
    ]
});

const vm = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
 */
// #endregion


// 路由传参
import Vue from '../lib/vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import app from '../components/ml/app.vue';
import list from '../components/ml/movieList.vue';
import target from '../components/ml/movieTarget.vue';

// 路由规则
const router = new VueRouter({
    routes: [{
            path: '/list',
            component: list
        },
        // {
        //     path: '/target/:id:name',
        //     component: target,
        // },
        {
            name: 'target',
            path: '/target/:id:name',
            component: target,
            props: true //指定props数组获取
        }
    ],
    linkActiveClass: 'active' //更改默认高亮类名
});

const vm = new Vue({
    el: '#app',
    router, //挂载路由
    render: h => h(list)
});