import Vue from '../lib/vue';
// 引入前端路由包 并配置
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import app from './app.vue';
import index from './index.vue';
import about from './about.vue';
import movie from './movie.vue';
// 路由嵌套
// import inner from './tabs/inner.vue';
// import outer from './tabs/outer.vue';
// 路由传参
import movieTarget from './movieTarget.vue';

// 写路由规则
const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            component: index
        },
        {
            path: '/about',
            component: about
        },
        {
            path: '/movie',
            component: movie,
            // redirect: '/movie/inner',
            // children: [{ //子路由
            //         path: '/movie/inner',
            //         component: inner
            //     },
            //     {
            //         path: '/movie/outer',
            //         component: outer
            //     }
            // ]
        },
        {
            // 命名路由 简化传递方法
            name: 'mm',
            path: '/movieTarget/:id',
            component: movieTarget,
            props: true //使用props数组接收 第一步
        }
    ],
    linkActiveClass: 'active' //修改路由高亮类
});

const vm = new Vue({
    el: '#app',
    router, //挂载路由
    render: h => h(app)
});