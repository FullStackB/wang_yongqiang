import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

// 路由表
const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [{
      path: "/",
      redirect: "/home"
    },
    {
      path: "/home",
      component: Home
    },
    {
      // 懒加载
      path: "/login",
      component: () => import("./views/Login.vue")
    }
  ]
});

// 路由守卫
router.beforeEach((to, path, next) => {
  // 登录页直接放行
  if (to.path == "/login") return next();
  // 其他页面检测token
  const token = window.sessionStorage.getItem('token');
  if (!token) return next("/login");

  next();
});

export default router;