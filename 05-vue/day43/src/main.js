import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// 配置element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 配置axios
import Axios from 'axios';
Axios.defaults.baseURL = 'https://www.liulongbin.top:8888/api/private/v1/'; //默认地址
Vue.prototype.$http = Axios;

Vue.config.productionTip = false;

import './assets/css/base.css';

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");