import Vue from '../lib/vue';


import app from './app.vue';


const vm = new Vue({
    el: '#app',
    render: h => h(app)
});