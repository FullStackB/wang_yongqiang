import Vue from '../lib/vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 引入模块
import list from '../components/list.vue';
import target from '../components/target.vue';

// 写路由规则
const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            component: list
        },
        {
            path: '/target/:id',
            component: target,
            props: true
        }
    ]
});

export default router;