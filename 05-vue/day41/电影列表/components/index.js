import Vue from '../lib/vue';

import app from './app.vue';

import router from '../router';

const vm = new Vue({
    el: '#app',
    router,
    render: h => h(app)
});