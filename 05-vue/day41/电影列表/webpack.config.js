const path = require('path');

// 导入插件
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');
const htmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const vuePlugin = new VueLoaderPlugin()

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, './dist'),
        filename: 'index.js'
    },
    module: {
        rules: [
            // 处理css代码
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            // 处理vue文件
            {
                test: /\.vue$/,
                use: 'vue-loader'
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new htmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html'
        }),
        vuePlugin
    ],
    devServer: {
        contentBase: './dist'
    }
}