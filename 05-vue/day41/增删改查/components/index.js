// 使用 json-server axios完成增删改查

// 增删改查  展示删除  修改  增加

import Vue from '../lib/vue';

import app from './app.vue';


// 引入路由
import router from '../router';

const vm = new Vue({
    el: '#app',
    router,
    render: h => h(app)
});