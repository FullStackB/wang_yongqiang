// 前端路由表
import Vue from '../lib/vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 引入组件
import index from '../components/index.vue';
import add from '../components/add.vue';
import update from '../components/update.vue';

// 写路由规则
const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            component: index
        },
        {
            path: '/add',
            component: add,
            props: true
        },
        {
            path: '/update/:id',
            component: update,
            props: true
        }
    ]
});

export default router;