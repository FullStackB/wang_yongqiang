import Vue from '../lib/vue';
// 引入并配置前端路由
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 引入模板
import app from './app.vue';
import index from './index.vue';
import login from './login.vue';

// 写路由规则
const router = new VueRouter({
    routes: [{ //重定向
            path: '/',
            redirect: '/index'
        },
        { //模板对应路由
            path: '/index',
            component: index
        },
        {
            path: '/login',
            component: login
        }
    ]
});

// 现有app模板 在app里去显示index模板 login模板
// #region 原来版本1
/**
 * // 路由守卫(导航守卫)
router.beforeEach(function (to, from, next) {
    // from 从那个网页跳过来的
    // to 要跳到那个网页 
    if (to.path == '/login') return next();
    // if (from.path == '/login') return next();

    // 获取令牌
    const token = window.sessionStorage.getItem('isLogin');
    if (!token) return next('/login');
    next();
});
 */
// #endregion

// 版本2
router.beforeEach(function (to, from, next) {
    // 路由守卫 他是检测所有路由 除要拦截路由外 其他路由也得处理
    if (to.path != '/index') return next();

    const token = window.sessionStorage.getItem('token');
    if (!token) {
        return next('/login');
    }
    next();
});

const vm = new Vue({
    el: '#app',
    router,
    render: h => h(app)
});