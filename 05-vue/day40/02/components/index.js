// 嵌套路由 movie movieList movieTarget
// 路由传参 movieList movieTarget
// 导航守卫 login movie

// 写嵌套路由 先写模板  再加children路由(父级需要 router-view 重定向)
// 写路由传参 先有跳转  router允许props数组接收 
// 路由守卫 所有路由在执行前都会调用的 

import Vue from '../lib/vue';
// 导入并配置路由包
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 引入组件
import app from './app.vue';
import login from './login.vue';
import movie from './movie.vue';
import movieList from './movieList.vue';
import movieTarget from './movieTarget.vue';

// 写路由规则
const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            component: movie,
            redirect: '/index/list',
            children: [{
                    path: '/index/list',
                    component: movieList
                },
                {
                    path: '/index/target/:id:name',
                    component: movieTarget,
                    props: true
                },
            ]
        },
        {
            path: '/login',
            component: login
        }
    ]
});

// 导航守卫
router.beforeEach(function (to, from, next) {
    // 只判断首页
    // if (to.path != '/' || to.path != '/index') return next();
    if (to.path == '/login') next();

    const token = window.sessionStorage.getItem('token');
    // 没有登录 跳去登录
    if (!token) return next('/login');

    next();
});

const vm = new Vue({
    el: '#app',
    router,
    render: h => h(app)
});