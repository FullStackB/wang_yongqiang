// 嵌套路由 movie movieList movieTarget
// 路由传参 movieList movieTarget
// 导航守卫 movie login

// 打开服务 就是电影页 和登录页切换
// 登录后跳转 电影页
// 在电影页 点li 跳目标页


import Vue from '../lib/vue';
// 引入并配置 前端路由包
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 引入组件
import app from './app.vue';
import login from './login.vue';
import movie from './movie.vue';
import movieList from './movieList.vue';
import movieTarget from './movieTarget.vue';

// 写路由规则
const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            component: movie,
            redirect: '/index/list',
            children: [{
                    path: '/index/list',
                    component: movieList
                },
                {
                    path: '/index/target/:id:name',
                    component: movieTarget,
                    props: true //允许target 使用props数组接收
                }
            ]
        },
        {
            path: '/login',
            component: login
        }
    ]
});

router.beforeEach(function (to, from, next) {
    // 之前 每一个
    // 进入之前 进入路由之前加个if 
    // 判断完成后之后两个操作 next()

    // 只验证首页
    if (to.path != '/index') next();

    const token = window.sessionStorage.getItem('token');
    if (!token) return next('/login');
    
    next(); //next()

    // 写路由判断 就是为了和浏览器端存储
});

const vm = new Vue({
    el: '#app',
    router,
    render: h => h(app)
});