import Vue from '../lib/vue';
// 引入前端路由包 并配置
import VueRouter from 'vue-router';
Vue.use(VueRouter);


// 嵌套路由 路由传参 导航守卫

// 引入模板
import app from './app.vue';
import index from './movie.vue';
import login from './login.vue';
import movieList from './movieList.vue';
import movieTarget from './movieTarget.vue';

// 写前端路由
const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            component: index,
            redirect: '/index/list', //路由重定向
            children: [{
                    path: '/index/list',
                    component: movieList
                },
                {
                    path: '/index/target/:id:name',
                    component: movieTarget,
                    props: true //允许使用props数组接收路由参数
                }
            ]
        },
        {
            path: '/login',
            component: login
        }
    ]
});


// 导航守卫
router.beforeEach((to, from, next) => {
    // 只判断首页
    if (to.path != '/index/list') return next();

    const token = window.sessionStorage.getItem('token');
    if (!token) return next('/login');

    next();
});

const vm = new Vue({
    el: '#app',
    router,
    render: h => h(app)
});