// 处理函数 模块

// 导入mysql
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: 'root',
    database: 'dany'
});

let showAdd = (req, res) => {
    res.render('add.html');
}

let showAdmin = (req, res) => {
    connection.query('select * from cc', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.render('admin.html', { list: result });
        }
    });
}

let adduser = (req, res) => {
    connection.query('insert into cc(username,password,sex,age) values("' + req.body.username + '","' + req.body.password + '","' + req.body.sex + '","' + req.body.age + '")', (err) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

let deluser = (req, res) => {
    connection.query('delete from cc where id =' + req.query.id, (err) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/admin');
}

module.exports = {
    showAdd,
    showAdmin,
    adduser,
    deluser
}