// 路由模块

const express = require('express');
const router = express();
// 导入 处理函数模块
const ctrl = require('../controller');

// 显示注册页
router.get('/', ctrl.showAdd);
// 显示管理页
router.get('/admin', ctrl.showAdmin); //redirect 只能转到get提交地址
// 增加数据
router.post('/adduser', ctrl.adduser);
// 删除数据
router.get('/deluser', ctrl.deluser);

module.exports = router;