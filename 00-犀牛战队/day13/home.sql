-- 水手订船
create table sailors(
  sid int primary key,
  sname varchar(20),
  rating int,
  age tinyint
);

-- 添加数据
insert into sailors values(22,'dustin',7,45);
insert into sailors values(29,'brustus',1,33);
insert into sailors values(31,'lubber',8,56);
insert into sailors values(32,'andy',11,26);
insert into sailors values(58,'rusty',10,36);
insert into sailors values(64,'horatio',7,35);
insert into sailors values(71,'zorba',10,35);
insert into sailors values(74,'horatio',9,35);
insert into sailors values(85,'art',6,26);
insert into sailors values(86,'john',4,17);
insert into sailors values(95,'bob',3,64);
insert into sailors values(96,'frodo',6,26);
insert into sailors values(98,'tom',6,17);

-- boats 船信息表
create table boats(
  bid int primary key,
  bname varchar(10),
  color varchar(10)
);

-- 添加信息
insert into boats values(101,'A','red');
insert into boats values(102,'B','green');
insert into boats values(103,'C','blue');
insert into boats values(104,'D','white');
insert into boats values(105,'E','red');
insert into boats values(106,'F','blue');
insert into boats values(107,'G','green');

-- reserves 水手订船信息表
create table reserves(
  sid int REFERENCES sailors,
  bid int REFERENCES boats,
  rdate_date datetime
);

-- 添加数据
insert into reserves values(22,101,'2010-01-08');
insert into reserves values(22,102,'2010-01-09');
insert into reserves values(29,103,'2010-01-09');
insert into reserves values(22,104,'2010-03-08');
insert into reserves values(22,103,'2010-03-10');
insert into reserves values(32,105,'2010-03-11');
insert into reserves values(32,106,'2010-03-18');
insert into reserves values(32,102,'2010-03-19');
insert into reserves values(58,104,'2010-03-20');
insert into reserves values(64,105,'2010-03-20');
insert into reserves values(95,101,'2010-04-02');
insert into reserves values(85,102,'2010-04-05');
insert into reserves values(22,101,'2010-04-07');
insert into reserves values(22,105,'2010-05-01');
insert into reserves values(22,106,'2010-06-18');
insert into reserves values(22,107,'2010-07-09');
insert into reserves values(32,105,'2010-08-06');
insert into reserves values(29,104,'2010-08-07');
insert into reserves values(64,103,'2010-09-05');
insert into reserves values(58,102,'2010-09-09');
insert into reserves values(64,104,'2010-11-03');
insert into reserves values(64,105,'2010-11-04');

-- 1.查找定了103号船的水手
select * from sailors where sid in (select sid from reserves where bid = 103);

-- 2.查找定了红色船水手的姓名
select sname from sailors where sid in (select sid from reserves where bid in (select bid from boats where color = 'red'));

-- 3.将年龄小于30的水手级别+1
update sailors set rating = rating+1 where age < 30;

-- 4.查找定了红色船而没有定绿色船的水手姓名
select sname from sailors where sid in (select sid from reserves where bid in (select bid from boats where color = 'red' && color != 'green'));

-- 5.查找没有定过船的水手信息
select * from sailors where sid <> all(select sid from reserves group by sid);

-- 6.查找定过船而没有定过红色船的水手信息
select sname from sailors where sid in (select sid from reserves where bid in (select bid from boats where color is not null && color != 'red'));

-- 7.查找没有定过红色船的水手信息
select sname from sailors where sid in (select sid from reserves where bid in (select bid from boats where color != 'red'));

-- 8.查找定过所有船的水手姓名和编号
select sname,sid from sailors where sid in (select sid from reserves where bid is not null);

-- 9.查找年龄最大的水手姓名和年龄
select sname,age from sailors where age = (select max(age) from sailors);

-- 10.统计水手表中每个级别组的平均年龄和级别组
select avg(age),rating from sailors group by rating;

-- 11.统计水手表中每个人数不少于2人的级别组中年 满18岁水手的平均年龄和级别组
select avg(age),rating from sailors where age > 18 group by rating having count(*) >= 2;

-- 12.统计水手表中每个级别组的人数
select count(*),rating from sailors group by rating;

-- 13.统计水手表中人数最少的级别组及人数
select count(*),rating from sailors group by rating 
having count(*) = (select min(ci) from (select count(*)as ci from sailors group by rating) as b);

-- 14.查找定过船而没有定过相同的船的水手姓名
-- 重复次数
select count(*),sid as qb,sid from reserves group by sid;
-- 不重复次数
select count(*) as dy,sid from (select sid,bid from reserves group by sid,bid) as a group by sid;
-- 合并重复次数表 不重复次数表 
select * from 
(select count(*) as qb,sid from reserves group by sid) as a,
(select count(*) as dy,sid from ((select sid,bid from reserves group by sid,bid) as x) group by sid) as b
where a.sid=b.sid;

-- 找两次数相等(没有定同一艘船的)
select sname from sailors where sid in 
(select sid  from 
(select * from 
(select count(*) as qb,sid from reserves group by sid) as a,
(select count(*) as dy,sid as ssid from ((select sid,bid from reserves group by sid,bid) as x) group by sid) as b
where a.sid=b.ssid) as temp
where dy = qb
);


-- 15.删除名字叫lubber的水手的定船信息.
delete from reserves where sid = (select sid from sailors where sname = 'lubber');