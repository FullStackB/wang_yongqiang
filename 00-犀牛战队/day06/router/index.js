// 路由模块
const ctrl = require('../controller');
const express = require('express');
const router = express.Router();

// 获取数据
router.get('/getuser', ctrl.getuser);
// 添加数据
router.post('/adduser', ctrl.adduser);

module.exports = router;