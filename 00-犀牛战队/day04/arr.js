
let lc = function (arr) {
    let newArr = [];//存重复元素
    // 遍历数组元素
    for (let i = 0; i < arr.length; i++) {
        let count = 0;//默认次数
        for (let j = 0; j < arr.length; j++) {// 找次数
            if (arr[j] == arr[i]) {
                count++;

                if (count > 1) {
                    // 重复元素丢数组
                    if (newArr.indexOf(arr[i]) == -1) {
                        newArr.push(arr[i]);
                    }
                    break;
                }
            }
        }
    }
    return newArr;
}

let arr = [1, 2, 4, 4, 3, 4, 3];

console.log(lc(arr));