-- #### 单表基础查询

-- 1) 查询没有上级的员工全部信息（mgr是空的）
select * from emp where mgr is null;
-- 2) 列出30号部门所有员工的姓名、薪资
select ename,sal from emp where deptno = 30;
-- 3)  查询姓名包含 'A'的员工姓名、部门名称 
select ename,dname from emp,dept where emp.deptno = dept.deptno;
-- 4) 查询员工“TURNER”的员工编号和薪资
select empno,sal from emp where ename = 'turner';
-- 5) -- 查询薪资最高的员工编号、姓名、薪资
select empno,ename,sal from emp where sal = (select max(sal) from emp);
-- 6) -- 查询10号部门的平均薪资、最高薪资、最低薪资
select avg(sal),max(sal),min(sal) from emp where deptno = 10;

-- #### 子查询

-- 1) 列出薪金比员工“TURNER”多的所有员工姓名（ename）、员工薪资（sal）
select ename,sal from emp where sal > (select sal from emp where ename = 'turner');
-- 2) 列出薪金高于公司平均薪金的所有员工姓名、薪金。
select ename,sal from emp where sal > (select avg(sal) from emp);
-- 3) 列出与“SCOTT”从事相同工作的所有员工姓名、工作名称(不展示Scott的姓名、工作)
select ename,job from emp where job = (select job from emp where ename = 'scott') and ename <> 'scott';
-- 4) 列出薪金高于30部门最高薪金的其他部门员工姓名、薪金、部门号。
select ename,sal,deptno from emp where sal > (select max(sal) from emp where deptno = 30) and deptno <> 30;
-- 5) -- 查询薪资最高的员工编号、姓名、薪资
select empno,ename,sal from emp where sal = (select max(sal) from emp);
-- 6) 列出薪金高于本部门平均薪金的所有员工姓名、薪资、部门号、部门平均薪资。
select avg(sal) avgsal,deptno from emp group by deptno;
select a.ename,a.sal,a.deptno,b.avgsal from emp a, (select avg(sal) avgsal,deptno from emp group by deptno) b 
where a.deptno = b.deptno and a.sal > b.avgsal;


-- #### 自连接
-- 1) 列出所有员工的姓名及其直接上级的姓名。
select empno,ename,mgr from emp;
select b.ename,a.ename from (select empno,ename,mgr from emp) a, (select empno,ename,mgr from emp where mgr is not null) b
where a.empno = b.mgr;

-- 2) 列出受雇日期早于其直接上级的所有员工编号、员工姓名、员工入职时间、上级姓名、上级入职时间
select empno,ename,hiredate from emp;
select a.empno,a.ename,a.hiredate,b.ename,b.hiredate from (select empno,ename,hiredate,mgr from emp) a, (select empno,ename,hiredate from emp) b
where a.mgr = b.empno and a.hiredate < b.hiredate;
-- #### 左右连接

-- 1) 查询所有的部门编号及部门下员工编号、员工姓名。
select a.empno,a.ename,b.deptno from emp a 
right join dept b
on a.deptno = b.deptno;
-- #### 函数应用

-- 1) 列出最低薪金大于1500的工作名称（job）以及最低薪金
select job,min(sal) minsal from emp group by job having minsal > 1500; 
-- 2) 列出在每个部门工作的员工数量、平均工资
select deptno,count(*),avg(sal) from emp group by deptno;
-- 3) -- 查询每个部门的部门号、最高薪资
select deptno,max(sal) from emp group by deptno;
-- 4) 查询每种工作的工作名称和最低工资
select job,min(sal) from emp group by job;
-- 5) 列出所有员工的员工姓名、年工资,按年薪从低到高排序。
select ename, (12*sal) from emp order by sal;  
-- 6) -- 查询每个部门中薪资最高的员工姓名、薪资、部门号
select ename,max(sal),deptno from emp group by deptno; 
-- 7) -- 查询不是领导的员工编号、员工姓名、员工职位
select empno,ename,job from emp where
empno in (select distinct empno from emp where empno <> all(select distinct mgr from emp where mgr is not null));



-- #### 多表查询

-- 1) 查询岗位（job）是 'CLERK' 的员工编号、员工姓名、所在部门名称
select a.empno,a.ename,b.dname from emp a, dept b 
where a.deptno = b.deptno and a.job = 'clerk';
-- 2) 列出所有员工的姓名、部门名称和工资。
select a.ename,b.dname,a.sal from emp a, dept b 
where a.deptno = b.deptno;
-- 3) 查询至少有一个员工的部门编号、员工数量
select deptno, count(*) y from emp group by deptno having y >= 1;
-- 4) -- 查询出没有员工的那个部门的部门编号和部门名称
select deptno from dept where deptno <>all(select distinct deptno from emp);
-- 5) 查询在部门“SALES”（销售部）工作的员工的姓名、部门编号
select ename,deptno from emp where job = 'sales';