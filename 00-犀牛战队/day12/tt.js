/**
 *
 *
1、node.js如何导入fs模块
    const fs = require('fs);
2、node.js中 require()和exports的区别
    require(引入)可以导入其他模块
    exports(暴露)可以让其他js文件访问自己
3、如何使用node.js运行文件app.js
    <1 在控制台进入app.js所在目录
    <2 node app.js即可
4、如何使用使用 npm 命令安装常用的 Node.js web框架模块 express
    npm install express
5、写出node.js的全局变量（2个及以上）
    global console process
6、写出node.js中异步读取和同步读取文件的方法
    fs.readFile(地址,编码,(err, result) => {})
    let result = fs.readFileSync(地址,编码);
7、写出node.js中异步写入内容和同步写入内容到文件的方法
    fs.writeFile(地址,数据, (err)=> {})
    fs.writeFileSync(地址,数据);
 */