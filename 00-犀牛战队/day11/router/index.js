// 路由模块
const express = require('express');
const router = express.Router();
const ctrl = require('../controller');

// 显示注册页
router.get('/', ctrl.showAdd);
// 查询数据
router.get('/getuser', ctrl.getuser);
// 添加数据
router.post('/adduser', ctrl.adduser);
// 删除数据
router.post('/deluser', ctrl.deluser);

module.exports = router;