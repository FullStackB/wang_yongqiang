// 服务器
const express = require('express');
const bodyParser = require('body-parser');
const router = require('./router');
const app = express();

// 配置bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
// 设置静态资源目录
app.use(express.static('./public'));
// 配置路由
app.use(router);

app.listen(3000, ()=> {
    console.log('http://127.0.0.1:3000');
});
