// 处理器模块
const path = require('path');
const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'dany'
});

// 显示注册页
let showAdd = function (req, res) {
    res.sendFile(path.join(__dirname, '../public/add.html'));
}
// 查询数据
let getuser = function (req, res) {
    connection.query('select * from cc', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 向网页返回
            res.send(result);
        }
    });
}
// 添加数据
let adduser = function (req, res) {
    connection.query('insert into cc(password,username) values("' + req.body.password + '","' + req.body.username + '")', (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回成功添加信号!
            res.send('ok');
        }
    });
}
// 删除数据
let deluser = function (req, res) {
    connection.query('delete from cc where id =' + req.body.id, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回删除成功信号!
            res.send('ok');
        }
    })
}

module.exports = {
    showAdd,
    getuser,
    adduser,
    deluser
}