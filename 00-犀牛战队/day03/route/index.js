// 路由模板

const express = require('express');
const ctrl = require('../controller');
const router = express.Router();

// 显示首页
router.get('/', ctrl.showIndex);
// 下载功能
router.post('/down', ctrl.down);

module.exports = router;