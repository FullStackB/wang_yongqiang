// 处理器模块

const path = require('path');
// 显示首页
module.exports.showIndex = (req, res) => {
    res.sendFile(path.join(__dirname, '../views/index.html'));
}
// 下载
module.exports.down = (req, res) => {
    res.download(path.join(__dirname, '../music/cc.flac'), '像我这样的人.flac', (err) => {
        if (err) {
            console.log(err);
        }
    });
}