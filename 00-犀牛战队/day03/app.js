// 服务器

const express = require('express');
const router = require('./route');
const app = express();

// 设置静态资源
app.use(express.static('public'));
// 配置路由
app.use(router);

app.listen(3000, () => {
    console.log('http://127.0.0.1:3000');
});
