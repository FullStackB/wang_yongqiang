
// 移除数组 arr 中的所有值与 item 相等的元素。
// 不要直接修改数组 arr，结果返回新的数组 

// 封装
function remove(arr, item) {
    var newArr = [];//返回的数组
    //完成代码部分

    // 把不等元素是的留下
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] != item) {
            newArr.push(arr[i]);
        }
    }
    return newArr;
}

// 测试
let arr = [1, 2, 3, 4, 4, 51, 1, 1, 13, 5, 6];
console.log(remove(arr, 1));