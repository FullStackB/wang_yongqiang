-- 1)统计订购了商品的总人数。
select count(*) from (select cid,count(cid) from orderitem group by cid) a;
-- 2)统计顾客号和所订购商品总数量
select cid,sum(count) from orderitem group by cid;

-- 1)查找没订购商品的顾客号和顾客名。
select cid,cname from customer where cid <> all(select distinct cid from orderitem);
-- 2)查找订购商品号'0001'商品数量最多的顾客号和顾客名
select cid,cname from customer 
where cid in (select cid from orderitem  
where pid = 'p001' and count = (select max(count) from orderitem where pid = 'p001'));
-- 3)统计至少订购2种商品的顾客id和顾客名。
select cid,cname from customer 
where cid in  
(select cid from orderitem group by cid having count(*) >= 2); 

-- 1)查找所有顾客号和顾客名以及他们购买的商品号
select a.cid,a.cname,b.pid from customer a, orderitem b
where a.cid = b.cid;
-- 1)查找订购了商品"p001"的顾客号和顾客名。
select cid,cname from customer where cid in
(select cid from orderitem where pid = 'p001');
-- 2)查找订购了商品号为"p001"或者"p002"的顾客号和顾客名。
select cid,cname from customer where cid in 
(select distinct cid from orderitem where pid in ('p001', 'p002'));
-- 3)查找年龄在30至40岁的顾客所购买的商品名及商品单价。
select a.cid,b.pid,c.pname,c.pirce from customer a,orderitem b,product c
where (a.age between 30 and 40) and a.cid = b.cid and b.pid = c.pid;
-- 4)查找女顾客购买的商品号，商品名和价格。
select a.pid,b.pname,b.pirce from orderitem a,product b 
where a.pid = b.pid and a.cid in (select cid from customer where sex = '女');